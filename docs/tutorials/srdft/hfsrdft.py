import daltonproject as dp

molecule, basis = dp.mol_reader('water.mol')
hfsrdft = dp.QCMethod('HFsrDFT', 'SRPBEGWS')
hfsrdft.range_separation_parameter(0.4)
prop = dp.Property(energy=True)
hfsrdft_result = dp.dalton.compute(molecule, basis, hfsrdft, prop)
print('HFsrPBE energy =', hfsrdft_result.energy)
# HFsrPBE energy = -73.502309263107

import pathlib  # noqa

import numpy as np  # noqa

assert pathlib.Path(hfsrdft_result.filename).name == '1179dc2d9ea1bd20cbb73e9868087c52757d570c'
np.testing.assert_allclose(hfsrdft_result.energy, -73.502309263107)
