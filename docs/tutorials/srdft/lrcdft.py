import daltonproject as dp

molecule, basis = dp.mol_reader('water.mol')
hfsrdft = dp.QCMethod('HFsrDFT', 'LRCPBEGWS')
hfsrdft.range_separation_parameter(0.4)
prop = dp.Property(energy=True)
hfsrdft_result = dp.dalton.compute(molecule, basis, hfsrdft, prop)
print('LRCPBE energy =', hfsrdft_result.energy)
# LRCPBE energy = -73.527807924557

import pathlib  # noqa

import numpy as np  # noqa

assert pathlib.Path(hfsrdft_result.filename).name == '4e751b8eb5c2cee96c7a5f7fd08ce818412925af'
np.testing.assert_allclose(hfsrdft_result.energy, -73.527807924557)
