import daltonproject as dp

molecule, basis = dp.mol_reader('pyridine.mol')
molecule.analyze_symmetry()
mp2srdft = dp.QCMethod('MP2srDFT', 'SRPBEGWS')
mp2srdft.range_separation_parameter(0.4)
prop = dp.Property(energy=True)
mp2srdft_result = dp.dalton.compute(molecule, basis, mp2srdft, prop)
print(mp2srdft_result.filename)
# ec4b3dbee5faa68aa0361c5107c0a9102b7cfff8
print('MP2srPBE energy =', mp2srdft_result.energy)
# MP2srPBE energy = -244.7774549828

import pathlib  # noqa

import numpy as np  # noqa

assert pathlib.Path(mp2srdft_result.filename).name == 'ec4b3dbee5faa68aa0361c5107c0a9102b7cfff8'
np.testing.assert_allclose(mp2srdft_result.energy, -244.7774549828)
