import daltonproject as dp

molecule, basis = dp.mol_reader('pyridine.mol')
molecule.analyze_symmetry()
cassrdft = dp.QCMethod('CASsrDFT', 'SRPBEGWS')
cassrdft.range_separation_parameter(0.4)
prop = dp.Property(energy=True)
mp2srdft_output = dp.dalton.OutputParser('ec4b3dbee5faa68aa0361c5107c0a9102b7cfff8')
nat_occs = mp2srdft_output.natural_occupations
electrons, cas, inactive = dp.natural_occupation.pick_cas_by_thresholds(nat_occs, 1.99, 0.01)
cassrdft.complete_active_space(electrons, cas, inactive)
cassrdft.input_orbital_coefficients(mp2srdft_output.orbital_coefficients)
cassrdft_result = dp.dalton.compute(molecule, basis, cassrdft, prop)
print('CASsrDFT energy =', cassrdft_result.energy)
# CASsrDFT energy = -244.761631596478

import numpy as np  # noqa

assert electrons == 4
assert cas == [0, 2, 0, 2]
assert inactive == [11, 1, 7, 0]
np.testing.assert_allclose(cassrdft_result.energy, -244.761631596478)
