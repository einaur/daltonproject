import daltonproject as dp

molecule = dp.Molecule(input_file='water.xyz')
basis = dp.Basis(basis='cc-pVDZ')

dft = dp.QCMethod('DFT', 'KT3')
prop = dp.Property(nmr_shieldings=True)
result = dp.dalton.compute(molecule, basis, dft, prop)
print('NMR shieldings =', result.nmr_shieldings)
# NMR shieldings = [389.5505  36.8488  36.8488]

import numpy as np  # noqa

np.testing.assert_allclose(result.nmr_shieldings, [389.5505, 36.8488, 36.8488])
