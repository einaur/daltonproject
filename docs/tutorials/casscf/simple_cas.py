import daltonproject as dp

molecule, basis = dp.mol_reader('pyridine.mol')

hf = dp.QCMethod('HF')
prop = dp.Property(energy=True)
hf_result = dp.dalton.compute(molecule, basis, hf, prop)
print('Hartree-Fock energy =', hf_result.energy)
# Hartree-Fock energy = -243.637999873274
print('Electrons =', hf_result.num_electrons)
# Electrons = 42

casscf = dp.QCMethod('CASSCF')
casscf.complete_active_space(2, 2, 20)
casscf.input_orbital_coefficients(hf_result.orbital_coefficients)
prop = dp.Property(energy=True)
casscf_result = dp.dalton.compute(molecule, basis, casscf, prop)
print('CASSCF energy =', casscf_result.energy)
# CASSCF energy = -243.642555771886

import numpy as np  # noqa

assert hf_result.num_electrons == 42
np.testing.assert_allclose(hf_result.energy, -243.637999873274)
np.testing.assert_allclose(casscf_result.energy, -243.642555771886)
