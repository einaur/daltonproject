"""Parse Dalton output files"""
#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

from typing import Dict, Union
import numpy as np
from qcelemental import PhysicalConstantsContext

from .. import program
from ..program import (HyperfineCouplings, NumBasisFunctions, NumOrbitals, OrbitalEnergies, OrbitalEnergy,
                       PolarizabilityGradients)

constants = PhysicalConstantsContext('CODATA2018')


class OutputParser(program.OutputParser):
    """Parse Molcas output files."""

    def __init__(self, filename: str) -> None:
        self._filename = filename

    @property
    def filename(self) -> str:
        """Name of the Molcas output files without the extension."""
        return self._filename

    @property
    def energy(self) -> float:
        raise NotImplementedError

    @property
    def dipole(self) -> np.ndarray:
        raise NotImplementedError

    @property
    def polarizability(self) -> np.ndarray:
        raise NotImplementedError

    @property
    def nmr_shieldings(self) -> np.ndarray:
        raise NotImplementedError

    @property
    def first_hyperpolarizability(self) -> np.ndarray:
        raise NotImplementedError

    @property
    def electronic_energy(self) -> float:
        raise NotImplementedError

    @property
    def nuclear_repulsion_energy(self) -> float:
        raise NotImplementedError

    @property
    def num_electrons(self) -> int:
        raise NotImplementedError
 
    @property
    def num_orbitals(self) -> NumOrbitals:
        raise NotImplementedError

    @property
    def num_basis_functions(self) -> NumBasisFunctions:
        raise NotImplementedError

    @property
    def mo_energies(self) -> OrbitalEnergies:
        raise NotImplementedError

    @property
    def homo_energy(self) -> OrbitalEnergy:
        raise NotImplementedError

    @property
    def lumo_energy(self) -> OrbitalEnergy:
        raise NotImplementedError

    @property
    def excitation_energies(self) -> np.ndarray:
        raise NotImplementedError

    @property
    def oscillator_strengths(self) -> np.ndarray:
        raise NotImplementedError

    @property
    def two_photon_tensors(self) -> np.ndarray:
        raise NotImplementedError

    @property
    def two_photon_strengths(self) -> np.ndarray:
        raise NotImplementedError

    @property
    def two_photon_cross_sections(self) -> np.ndarray:
        raise NotImplementedError

    @property
    def orbital_coefficients(self) -> Union[Dict[int, np.ndarray], np.ndarray]:
        raise NotImplementedError

    @property
    def natural_occupations(self) -> Dict[int, np.ndarray]:
        raise NotImplementedError

    @property
    def hyperfine_couplings(self) -> HyperfineCouplings:
        raise NotImplementedError

    @property
    def gradients(self) -> np.ndarray:
        raise NotImplementedError

    @property
    def hessian(self) -> np.ndarray:
        raise NotImplementedError

    @property
    def dipole_gradients(self) -> np.ndarray:
        raise NotImplementedError

    @property
    def polarizability_gradients(self) -> PolarizabilityGradients:
        raise NotImplementedError

    @property
    def final_geometry(self) -> np.ndarray:
        raise NotImplementedError
