import logging
import os
import shutil
from typing import Optional
from qcelemental import PhysicalConstantsContext

from ..basis import Basis
from ..environment import Environment
from ..molecule import Molecule
from ..program import ComputeSettings, Program
from ..property import Property
from ..qcmethod import QCMethod
from ..utilities import chdir, num_cpu_cores, run, unique_filename
from .output_parser import OutputParser

logger = logging.getLogger('daltonproject')
constants = PhysicalConstantsContext('CODATA2018')

class Molcas(Program):
    """Interface to the Molcas program."""

    program_name: str = 'pymolcas'

    @classmethod
    def compute(cls,
                molecule: Molecule,
                basis: Basis,
                qc_method: QCMethod,
                properties: Property,
                environment: Optional[Environment] = None,
                compute_settings: Optional[ComputeSettings] = None,
                filename: Optional[str] = None,
                verbose = True,
                force_recompute: Optional[bool] = False) -> OutputParser:
        """Run a calculation using the OpenMolcas program.

        Args:
            molecule: Molecule on which a calculations is performed. This can
                also be an atom, a fragment, or any collection of atoms.
            basis: Basis set to use in the calculation.
            qc_method: Quantum chemistry method, e.g., HF, DFT, or CC, and
                associated settings.
            properties: Properties of molecule to be calculated, geometry
                optimization, excitation energies, etc.
            environment: TODO: Environment ...
            compute_settings: Settings for the calculation, e.g., number of MPI
                processes and OpenMP threads, work and scratch directory, etc.
            filename: Optional user-specified filename that will be used for
                input and output files. If not specified a name will be
                generated as a hash of the input.
            force_recompute: Recompute even if the output files already exist.

        Returns:
            OutputParser instance that contains the filename of the output
            produced in the calculation and can be used to extract results from
            the output.
        """
        if not verbose:
            logger.setLevel(logging.ERROR)

        Program._validate_compute_args(molecule, basis, qc_method, properties, environment, compute_settings,
                                       filename, force_recompute)
        input_string = molcas_input(molecule, basis, properties)
        if compute_settings is None:
            mpi_num_procs = num_cpu_cores()
            compute_settings = ComputeSettings(mpi_num_procs=mpi_num_procs)

        if not filename:
            input_list = [input_string]
            filename = unique_filename(input_list)

        filepath = os.path.join(compute_settings.work_dir, filename)

        if os.path.isfile(f'{filepath}.log') and os.path.isdir(f'{filepath}') and not force_recompute:
            logger.info('Output files already exist and will be reused.')
            return OutputParser(filepath)
        if shutil.which(cmd=cls.program_name) is None:
            raise FileNotFoundError(f'The {cls.program_name} script was not found or is not executable.')
        molcas_launcher = os.environ['MOLCAS']

        with chdir(compute_settings.work_dir):
            with open(f'{filename}.input', 'w') as molcas_file:
                molcas_file.write(input_string)

            command = (f'env MOLCAS_LAUNCHER="{molcas_launcher}"'
                       f' MOLCAS_WORKDIR="{compute_settings.work_dir}"'
                       f' MOLCAS_KEEP_WORKDIR="YES"'
                       f' {cls.program_name}'
                       f' -o {filename}.log'
                       f' {filename}.input')
            command = command.replace(' mpirun -np 4o','')
            stdout, stderr, return_code = run(command)

        if return_code != 0:
            raise Exception(f'OpenMolcas exited with non-zero return code ({return_code}). Stderr:\n{stderr}')

        return OutputParser(filepath)


def read_basis_file(el_basis):
    file0 = open(el_basis,'r')
    el_basis_str = file0.read()
    file0.close()
    return el_basis_str

def molcas_input(molecule: Molecule, basis: Basis, properties: Property) -> str:
    """Generate Molcas input file as a string
    """
    import basis_set_exchange as bse

    str0 = '''&GATEWAY
expert'''
    str1 = '''
&Seward
OneOnly
Center
 1
 2  0. 0. 0.'''


    coords = molecule.coordinates# * constants.conversion_factor('angstrom', 'bohr')

    c = 0
    for el in molecule.elements:
        if type(basis.basis) is str:
            el_basis = basis.basis
        else:
            el_basis = basis.basis[el]

        if basis.custom_basis:
            el_basis_str = read_basis_file(el_basis)
            #el_basis_str = basis_list[c]
        else:
            el_basis_str = bse.get_basis(el_basis,el,fmt='molcas')
            #el_basis_str = el_basis_str.replace('Basis set','')
            el_basis_str = '\n'.join(el_basis_str.split('\n')[15:])

        str0 += '\nBasis set'
        str0 += '\n '+el+'    / inline'
        str0 += '\n'+el_basis_str
        str0 += 'A'+str(c)+'   '+str(coords[c,0])+'   '+str(coords[c,1])+'   '+str(coords[c,2])
        str0 += '\nEnd of basis set'


        c += 1

    str0 += '\nAngmom'
    str0 += '\n 0. 0. 0.'
    str0 += '\nEMFR'

    k_direction = properties.settings['vector_potential']
    wavelength = properties.settings['vector_potential_wavelength']

    str0 += '\n'+str(k_direction[0])+' '+str(k_direction[1])+' '+str(k_direction[2])+'  '+str(wavelength)+' bohr'

    str0 += str1
    return str0
