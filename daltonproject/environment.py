#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

from abc import ABC, abstractmethod


class Environment(ABC):
    """Environment base class."""

    @abstractmethod
    def __init__(self, model: str) -> None:
        """Initialize Environment instance.

        Args:
            model: Specify the environment model.
        """
        self.model = model

    @abstractmethod
    def get_input_string(self, input_format: str) -> str:
        """Return the input in the specified format.

        This is needed by the compute() functions to produce unique filenames.

        Args:
            input_format: Format of the input.
        """
        pass

    @abstractmethod
    def write_input_files(self, filename: str, input_format: str) -> None:
        """Write input files needed to run environment calculation.

        Args:
            filename: Name of the environment file without extension.
            input_format: Format of the input.
        """
        pass

    @property
    def model(self) -> str:
        """Environment model."""
        return self._model

    @model.setter
    def model(self, model: str) -> None:
        if not isinstance(model, str):
            raise TypeError(f'Argument "model" must be of type str (not {type(model).__name__}).')
        self._model = model
