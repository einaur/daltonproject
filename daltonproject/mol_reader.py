#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

from typing import Dict, Tuple

import numpy as np
from qcelemental import PhysicalConstantsContext, periodictable

from .basis import Basis
from .molecule import Molecule

constants = PhysicalConstantsContext('CODATA2018')


def mol_reader(filename: str) -> Tuple[Molecule, Basis]:
    """Read Dalton molecule input file.

    Args:
        filename: Name of Dalton molecule input file containing atoms and coordinates.

    Returns:
        Molecule and Basis objects.
    """
    with open(f'{filename}', 'r') as mol_file:
        lines = mol_file.read().split('\n')
    with_generator = False
    with_atombasis = False
    if 'atombasis' in lines[0].lower():
        with_atombasis = True
        basis_set: Dict[str, str] = {}
    else:
        basis_set = {'without_atombasis': lines[1]}
    if with_atombasis:
        line_list = lines[3].lower().replace('=', ' ').split()
    else:
        line_list = lines[4].lower().replace('=', ' ').split()
    # Finding the charge of the molecule.
    if 'charge' in line_list:
        charge = int(float(line_list[line_list.index('charge') + 1]))
    else:
        charge = 0
    # Finding the unit of the coordinates
    if 'angstrom' in line_list:
        coordinate_unit = 'angstrom'
    else:
        coordinate_unit = 'bohr'
    if 'generators' in line_list:
        with_generator = True
        num_generators = int(float(line_list[line_list.index('generators') + 1]))
        x1, y1, z1, x2, y2, z2, x3, y3, z3 = 1, 1, 1, 1, 1, 1, 1, 1, 1
        if num_generators >= 1:
            generator_1 = line_list[line_list.index('generators') + 2]
            if 'x' in generator_1:
                x1 = -1
            if 'y' in generator_1:
                y1 = -1
            if 'z' in generator_1:
                z1 = -1
        if num_generators >= 2:
            generator_2 = line_list[line_list.index('generators') + 3]
            if 'x' in generator_2:
                x2 = -1
            if 'y' in generator_2:
                y2 = -1
            if 'z' in generator_2:
                z2 = -1
        if num_generators >= 3:
            generator_3 = line_list[line_list.index('generators') + 4]
            if 'x' in generator_3:
                x3 = -1
            if 'y' in generator_3:
                y3 = -1
            if 'z' in generator_3:
                z3 = -1
    # Read coordinates and labels (and basis set if ATOMBASIS).
    atom_counter = 0
    num_atoms = 0
    current_basis = ''
    atom_lines = ''
    if with_atombasis:
        start_idx = 3
    else:
        start_idx = 4
    if not with_generator:
        for line in lines[start_idx:]:
            if 'atoms' in line.lower():
                line_list_original = line.replace('=', ' ').split()
                line_list = line.lower().replace('=', ' ').split()
                # Find which element it is.
                current_element = periodictable.to_symbol(int(float(line_list[line_list.index('charge') + 1])))
                if with_atombasis:
                    # Determine basis set for coming labels.
                    # Basis set needs to be case sensitive.
                    # Therefore 'line_list_original' is used here.
                    current_basis = line_list_original[line_list.index('basis') + 1]
                # Reset atom_counter.
                atom_counter = 0
                num_atoms = int(float(line_list[line_list.index('atoms') + 1]))
            elif atom_counter < num_atoms:
                atom_counter += 1
                coordinate = np.array([float(line.split()[1]),
                                       float(line.split()[2]),
                                       float(line.split()[3])]) * constants.conversion_factor(
                                           coordinate_unit, 'angstrom')
                label = line.split()[0]
                atom_lines += f'{current_element} {coordinate[0]} {coordinate[1]} {coordinate[2]} {label};'
                if with_atombasis:
                    basis_set[label] = current_basis
    else:
        # Handle reading coordinates from generator.
        for line in lines[start_idx:]:
            if 'atoms' in line.lower():
                line_list_original = line.replace('=', ' ').split()
                line_list = line.lower().replace('=', ' ').split()
                # Find which element it is.
                current_element = periodictable.to_symbol(int(float(line_list[line_list.index('charge') + 1])))
                if with_atombasis:
                    # Determine basis set for coming labels.
                    # Basis set needs to be case sensitive.
                    # Therefore 'line_list_original' is used here.
                    current_basis = line_list_original[line_list.index('basis') + 1]
                # Reset atom_counter.
                atom_counter = 0
                num_atoms = int(float(line_list[line_list.index('atoms') + 1]))
            elif atom_counter < num_atoms:
                atom_counter += 1
                coordinate = np.array([float(line.split()[1]),
                                       float(line.split()[2]),
                                       float(line.split()[3])]) * constants.conversion_factor(
                                           coordinate_unit, 'angstrom')
                label = line.split()[0]
                atom_lines += f'{current_element} {coordinate[0]} {coordinate[1]} {coordinate[2]} {label};'
                if with_atombasis:
                    basis_set[label] = current_basis
                found_coordinates = [coordinate.tolist()]
                next_coordinate = np.array([x1 * coordinate[0], y1 * coordinate[1], z1 * coordinate[2]])
                # The if statement is to ensure that the generator wont place an atom on top of another atom.
                if np.all(np.sum((found_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    atom_lines += f'{current_element} {next_coordinate[0]} {next_coordinate[1]}' \
                                  f' {next_coordinate[2]} {label};'
                    found_coordinates.append(next_coordinate.tolist())
                next_coordinate = np.array([x2 * coordinate[0], y2 * coordinate[1], z2 * coordinate[2]])
                if np.all(np.sum((found_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    atom_lines += f'{current_element} {next_coordinate[0]} {next_coordinate[1]}' \
                                  f' {next_coordinate[2]} {label};'
                    found_coordinates.append(next_coordinate.tolist())
                next_coordinate = np.array([x3 * coordinate[0], y3 * coordinate[1], z3 * coordinate[2]])
                if np.all(np.sum((found_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    atom_lines += f'{current_element} {next_coordinate[0]} {next_coordinate[1]}' \
                                  f' {next_coordinate[2]} {label};'
                    found_coordinates.append(next_coordinate.tolist())
                next_coordinate = np.array(
                    [x1 * x2 * coordinate[0], y1 * y2 * coordinate[1], z1 * z2 * coordinate[2]])
                if np.all(np.sum((found_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    atom_lines += f'{current_element} {next_coordinate[0]} {next_coordinate[1]}' \
                                  f' {next_coordinate[2]} {label};'
                    found_coordinates.append(next_coordinate.tolist())
                next_coordinate = np.array(
                    [x1 * x3 * coordinate[0], y1 * y3 * coordinate[1], z1 * z3 * coordinate[2]])
                if np.all(np.sum((found_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    atom_lines += f'{current_element} {next_coordinate[0]} {next_coordinate[1]}' \
                                  f' {next_coordinate[2]} {label};'
                    found_coordinates.append(next_coordinate.tolist())
                next_coordinate = np.array(
                    [x2 * x3 * coordinate[0], y2 * y3 * coordinate[1], z2 * z3 * coordinate[2]])
                if np.all(np.sum((found_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    atom_lines += f'{current_element} {next_coordinate[0]} {next_coordinate[1]}' \
                                  f' {next_coordinate[2]} {label};'
                    found_coordinates.append(next_coordinate.tolist())
                next_coordinate = np.array([
                    x1 * x2 * x3 * coordinate[0],
                    y1 * y2 * y3 * coordinate[1],
                    z1 * z2 * z3 * coordinate[2],
                ])
                if np.all(np.sum((found_coordinates - next_coordinate)**2, axis=1)**2 > 1e-3):
                    atom_lines += f'{current_element} {next_coordinate[0]} {next_coordinate[1]}' \
                                  f' {next_coordinate[2]} {label};'
                    found_coordinates.append(next_coordinate.tolist())
    # The -1 is to remove the trailing ";".
    molecule = Molecule(atoms=atom_lines[:-1], charge=charge)
    if with_atombasis:
        basis = Basis(basis=basis_set)
    else:
        basis = Basis(basis=basis_set['without_atombasis'])
    return molecule, basis
