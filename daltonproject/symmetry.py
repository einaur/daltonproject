#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/
from collections import namedtuple
from typing import Dict, List, Optional, Tuple

import numpy as np
from qcelemental import periodictable

SUPERGROUPS = {
    'Cs': ['Cs', 'C3h', 'C5h', 'C7h', 'C9h', 'C11h', 'C13h', 'C15h', 'C17h', 'C19h'],
    'C3v': ['C3v', 'C5v', 'C7v', 'C9v', 'C11v', 'C13v', 'C15v', 'C17v', 'C19v'],
    'C2': ['C2', 'C4', 'C6', 'C8', 'C10', 'C12', 'C14', 'C16', 'C18', 'C20', 'S4', 'S8', 'S12', 'S16', 'S20'],
    'D3': ['D3', 'D5', 'D7', 'D9', 'D11', 'D13', 'D15', 'D17', 'D19'],
    'Ci': ['Ci', 'S6', 'S10', 'S14', 'S18'],
    'C2v': [
        'C2v', 'C4v', 'C6v', 'C8v', 'C10v', 'C12v', 'C14v', 'C16v', 'C18v', 'C20v', 'D2d', 'D4d', 'D6d', 'D8d',
        'D10d', 'D12d', 'D14d', 'D16d', 'D18d', 'D20d', 'Coov'
    ],
    'D3h': ['D3h', 'D5h', 'D7h', 'D9h', 'D11h', 'D13h', 'D15h', 'D17h', 'D19h'],
    'C2h': ['C2h', 'C4h', 'C6h', 'C8h', 'C10h', 'C12h', 'C14h', 'C16h', 'C18h', 'C20h'],
    'D3d': ['D3d', 'D5d', 'D7d', 'D9d', 'D11d', 'D13d', 'D15d', 'D17d', 'D19d'],
    'D2': ['D2', 'D4', 'D6', 'D8', 'D10', 'D12', 'D14', 'D16', 'D18', 'D20', 'T', 'Td', 'O', 'I'],
    'D2h': [
        'D2h', 'D4h', 'D6h', 'D8h', 'D10h', 'D12h', 'D14h', 'D16h', 'D18h', 'D20h', 'Th', 'Oh', 'Ih', 'Dooh', 'K'
    ],
    'C1': ['C1', 'C3', 'C5', 'C7', 'C9', 'C11', 'C13', 'C15', 'C17', 'C19'],
}
# C1 are point groups that will be represented as C1 in Dalton.


class SymmetryAnalysis:
    """Class for determining the symmetry of a molecule.

    Attributes:
        symmetry_threshold (float): Threshold for determination of symmetry.
                                    Default is 1e-3.
        max_rotations (int): Maximum order of rotation to be checked for.
                             Default and maximum value is 20.
        xyz (np.ndarray): Cartesian coordinates of molecule.
        symmetry_elements (Dict[str, np.ndarray]): Symmetry elements of molecule.
    """

    def __init__(self,
                 elements: List[str],
                 xyz: np.ndarray,
                 symmetry_threshold: float = 1e-3,
                 labels: Optional[List[str]] = None) -> None:
        """Initialize SymmetryAnalysis instance.

        Args:
            elements: Elements.
            xyz: Cartesian coordinates of the atoms.
            symmetry_threshold: Geometrical threshold for detection of symmetry.
        """
        self.symmetry_threshold = symmetry_threshold
        self.max_rotation_order = 20
        self.same_axis_threshold = 1e-3
        self.mass = np.array([periodictable.to_mass(element) for element in elements])
        if labels is not None:
            self.labels = labels
        else:
            self.labels = np.copy(self.mass, dtype=str).tolist()
        self.symmetry_elements: Dict[str, np.ndarray] = {}
        # Move molecule to center of mass.
        self._xyz = xyz
        self._xyz = self._xyz - self._calculate_center_of_mass()

        self.moments_of_inertia = self._calculate_moment_of_inertia_tensor()
        self.principal_moment_of_inertia = self._calculate_principal_axes_of_inertia()
        self.point_group = ''
        self.principal_axis_order = 0
        self.allocation_size = max(2 * int(len(elements) * (len(elements) - 1)), 100)

    def find_symmetry(self) -> Tuple[np.ndarray, str]:
        """Find the symmetry of the molecule.

        Returns:
            Coordinates and point group of the molecule
        """
        # Check if the molecule is in a special symmetry class based in the principal axis of inertia.
        self.special_group = False
        self.special_group_type = 0
        if abs(np.mean(self.principal_moment_of_inertia[0])) > 1e-6:
            relative_principal_moment_of_inertia = self.principal_moment_of_inertia[0] / np.mean(
                self.principal_moment_of_inertia[0])
        else:
            relative_principal_moment_of_inertia = self.principal_moment_of_inertia[0]
        if abs(relative_principal_moment_of_inertia[0]) < self.symmetry_threshold\
                or abs(relative_principal_moment_of_inertia[1]) < self.symmetry_threshold\
                or abs(relative_principal_moment_of_inertia[2]) < self.symmetry_threshold:
            # linear or sphere.
            self.special_group = True
            self.special_group_type = 1
        elif abs(relative_principal_moment_of_inertia[0]
                 - relative_principal_moment_of_inertia[1])\
                < self.symmetry_threshold and abs(relative_principal_moment_of_inertia[0]
                                                  - relative_principal_moment_of_inertia[2])\
                < self.symmetry_threshold:
            # Td, Th, T, Oh, Ih, I or O.
            self.special_group = True
            self.special_group_type = 2
        # Grouping the atoms such that only atoms with same label and same distance to origin is in the same group.
        self.atom_groups = {}
        distances_list = []
        labels_list = []
        group_counter = 0
        for i, coordinates in enumerate(self._xyz):
            distances_to_origin = np.linalg.norm(coordinates)
            if i == 0:
                # The first one is never in an existing group, and the lists
                # need at least one value to be able to run the rest of the grouping.
                self.atom_groups[group_counter] = [coordinates]
                labels_list.append(self.labels[i])
                distances_list.append(distances_to_origin)
                group_counter += 1
            elif np.any(np.abs(np.array(distances_list) - distances_to_origin) < self.symmetry_threshold):
                idx = np.argmin(np.abs(np.array(distances_list) - distances_to_origin))
                if self.labels[i] == labels_list[idx]:
                    self.atom_groups[idx].append(coordinates)
                else:
                    self.atom_groups[group_counter] = [coordinates]
                    labels_list.append(self.labels[i])
                    distances_list.append(distances_to_origin)
                    group_counter += 1
            else:
                self.atom_groups[group_counter] = [coordinates]
                labels_list.append(self.labels[i])
                distances_list.append(distances_to_origin)
                group_counter += 1
        # Check if the molecule have inversion.
        self.inversion = self._check_inversion()
        if self.inversion:
            self.symmetry_elements['inversion'] = True
        # Check for proper rotations.
        self._find_proper_rotations()
        self._assign_proper_rotations_symmetry_elements()
        # Check for improper rotations.
        self._find_improper_rotations()
        self._assign_improper_rotations_symmetry_elements()
        # Check for reflections.
        self._find_reflections()
        self._assign_reflections_symmetry_elements()
        # Assign point group.
        self._assign_point_group()
        output = namedtuple('Symmetry', ['coordinates', 'point_group'])
        return output(self._xyz, self.point_group)

    def _assign_point_group(self) -> None:
        """Assigns the point group of the molecule."""
        # If special symmetry group.
        if len(self._xyz) == 1:
            self.point_group = 'K'
        elif self.special_group:
            if self.special_group_type == 1:
                if len(self.symmetry_elements[f'C{self.max_rotation_order}']) == 1\
                        and len(self.symmetry_elements['sigma_h']) == 1:
                    assert len(self.symmetry_elements[f'C{self.max_rotation_order}']) == 1
                    assert len(self.symmetry_elements[f'S{2 * self.max_rotation_order}']) == 1
                    assert len(self.symmetry_elements['sigma_h']) == 1
                    assert self.inversion
                    self.point_group = 'Dooh'
                elif len(self.symmetry_elements[f'C{self.max_rotation_order}']) == 1:
                    assert len(self.symmetry_elements[f'C{self.max_rotation_order}']) == 1
                    assert not self.inversion
                    assert len(self.improper_rotation_list) == 0
                    self.point_group = 'Coov'
            elif self.special_group_type == 2:
                if len(self.symmetry_elements['C2']) == 3 and len(self.symmetry_elements['C3']) == 4\
                        and len(self.symmetry_elements['sigma_h']) == 3:
                    assert len(self.symmetry_elements['C2']) == 3
                    assert len(self.symmetry_elements['C3']) == 4
                    assert len(self.symmetry_elements['sigma_h']) == 3
                    assert self.inversion
                    assert len(self.symmetry_elements['S6']) == 4
                    self.point_group = 'Th'
                elif len(self.symmetry_elements['C2']) == 3 and len(self.symmetry_elements['C3']) == 4\
                        and len(self.symmetry_elements['sigma_d']) == 6:
                    assert len(self.symmetry_elements['C2']) == 3
                    assert len(self.symmetry_elements['C3']) == 4
                    assert len(self.symmetry_elements['sigma_d']) == 6
                    assert len(self.symmetry_elements['S4']) == 3
                    assert not self.inversion
                    self.point_group = 'Td'
                elif len(self.symmetry_elements['C2']) == 3 and len(self.symmetry_elements['C3']) == 4:
                    assert len(self.symmetry_elements['C2']) == 3
                    assert len(self.symmetry_elements['C3']) == 4
                    assert not self.inversion
                    assert len(self.reflection_list) == 0
                    assert len(self.improper_rotation_list) == 0
                    self.point_group = 'T'
                elif len(self.symmetry_elements['C2']) == 6 and len(self.symmetry_elements['C3']) == 4\
                        and len(self.symmetry_elements['C4']) == 3 and len(self.symmetry_elements['sigma_d']) == 3:
                    assert len(self.symmetry_elements['C2']) == 6
                    assert len(self.symmetry_elements['C3']) == 4
                    assert len(self.symmetry_elements['C4']) == 3
                    assert self.inversion
                    assert len(self.symmetry_elements['S4']) == 3
                    assert len(self.symmetry_elements['S6']) == 4
                    assert len(self.symmetry_elements['sigma_d']) == 3
                    assert len(self.symmetry_elements['sigma_h']) == 6
                    self.point_group = 'Oh'
                elif len(self.symmetry_elements['C2']) == 6 and len(self.symmetry_elements['C3']) == 4\
                        and len(self.symmetry_elements['C4']) == 3:
                    assert len(self.symmetry_elements['C2']) == 6
                    assert len(self.symmetry_elements['C3']) == 4
                    assert len(self.symmetry_elements['C4']) == 3
                    assert not self.inversion
                    assert len(self.reflection_list) == 0
                    assert len(self.improper_rotation_list) == 0
                    self.point_group = 'O'
                elif len(self.symmetry_elements['C2']) == 15 and len(self.symmetry_elements['sigma_h']) == 15:
                    assert len(self.symmetry_elements['C2']) == 15
                    assert len(self.symmetry_elements['C3']) == 10
                    assert len(self.symmetry_elements['C5']) == 6
                    assert self.inversion
                    assert len(self.symmetry_elements['S6']) == 10
                    assert len(self.symmetry_elements['S10']) == 6
                    assert len(self.symmetry_elements['sigma_h']) == 15
                    self.point_group = 'Ih'
                elif len(self.symmetry_elements['C2']) == 15:
                    assert len(self.symmetry_elements['C2']) == 15
                    assert len(self.symmetry_elements['C3']) == 10
                    assert len(self.symmetry_elements['C5']) == 6
                    assert not self.inversion
                    assert len(self.reflection_list) == 0
                    assert len(self.improper_rotation_list) == 0
                    self.point_group = 'I'
        # If lower symmetry group, Ci, Cs or C1.
        elif len(self.proper_rotation_list) == 0 and len(self.improper_rotation_list) == 0:
            if self.inversion:
                assert self.inversion
                self.point_group = 'Ci'
            elif len(self.reflection_list) == 1:
                assert len(self.reflection_list) == 1
                assert not self.inversion
                assert len(self.proper_rotation_list) == 0
                assert len(self.improper_rotation_list) == 0
                self.point_group = 'Cs'
            else:
                assert not self.inversion
                assert len(self.reflection_list) == 0
                assert len(self.proper_rotation_list) == 0
                assert len(self.improper_rotation_list) == 0
                self.point_group = 'C1'
        else:
            if len(self.symmetry_elements['C2p']) + len(self.symmetry_elements['C2pp']) == self.principal_axis_order:
                if len(self.symmetry_elements['sigma_h']) == 1:
                    if self.principal_axis_order % 2 == 0:
                        assert len(self.symmetry_elements['C2p']) == self.principal_axis_order // 2
                        assert len(self.symmetry_elements['C2pp']) == self.principal_axis_order // 2
                        assert self.inversion
                        assert len(self.symmetry_elements['sigma_v']) == self.principal_axis_order // 2
                        assert len(self.symmetry_elements['sigma_d']) == self.principal_axis_order // 2
                    else:
                        assert len(self.symmetry_elements['C2p']) == self.principal_axis_order
                        assert len(self.symmetry_elements['sigma_v']) == self.principal_axis_order
                        assert not self.inversion
                    assert len(self.symmetry_elements[f'C{self.principal_axis_order}']) == 1
                    if self.principal_axis_order > 2:
                        assert len(self.symmetry_elements[f'S{self.principal_axis_order}']) == 1
                    assert len(self.symmetry_elements['sigma_h']) == 1
                    self.point_group = f'D{self.principal_axis_order}h'
                elif len(self.symmetry_elements['sigma_d']) == self.principal_axis_order:
                    if self.principal_axis_order % 2 == 0:
                        assert len(self.symmetry_elements['C2p']) == self.principal_axis_order // 2
                        assert len(self.symmetry_elements['C2pp']) == self.principal_axis_order // 2
                        assert not self.inversion
                    else:
                        assert len(self.symmetry_elements['C2p']) == self.principal_axis_order
                        assert self.inversion
                    assert len(self.symmetry_elements[f'C{self.principal_axis_order}']) == 1
                    assert len(self.symmetry_elements[f'S{2 * self.principal_axis_order}']) == 1
                    assert len(self.symmetry_elements['sigma_d']) == self.principal_axis_order
                    self.point_group = f'D{self.principal_axis_order}d'
                else:
                    if self.principal_axis_order % 2 == 0:
                        assert len(self.symmetry_elements['C2p']) == self.principal_axis_order // 2
                        assert len(self.symmetry_elements['C2pp']) == self.principal_axis_order // 2
                    else:
                        assert len(self.symmetry_elements['C2p']) == self.principal_axis_order
                    assert len(self.symmetry_elements[f'C{self.principal_axis_order}']) == 1
                    assert not self.inversion
                    assert len(self.reflection_list) == 0
                    assert len(self.improper_rotation_list) == 0
                    self.point_group = f'D{self.principal_axis_order}'
            else:
                if len(self.symmetry_elements['sigma_h']) == 1:
                    assert len(self.symmetry_elements[f'C{self.principal_axis_order}']) == 1
                    assert len(self.symmetry_elements['sigma_h']) == 1
                    if self.principal_axis_order > 2:
                        assert len(self.symmetry_elements[f'S{self.principal_axis_order}']) == 1
                        assert not self.inversion
                    else:
                        assert self.inversion
                        assert len(self.improper_rotation_list) == 0
                    self.point_group = f'C{self.principal_axis_order}h'
                elif len(self.symmetry_elements['sigma_v']) == self.principal_axis_order:
                    assert len(self.symmetry_elements[f'C{self.principal_axis_order}']) == 1
                    assert len(self.symmetry_elements['sigma_v']) == self.principal_axis_order
                    assert not self.inversion
                    assert len(self.improper_rotation_list) == 0
                    self.point_group = f'C{self.principal_axis_order}v'
                elif f'S{2 * self.principal_axis_order}' in self.symmetry_elements:
                    assert len(self.symmetry_elements[f'C{self.principal_axis_order}']) == 1
                    assert len(self.symmetry_elements[f'S{2 * self.principal_axis_order}']) == 1
                    assert not self.inversion
                    assert len(self.reflection_list) == 0
                    self.point_group = f'S{2 * self.principal_axis_order}'
                else:
                    assert len(self.symmetry_elements[f'C{self.principal_axis_order}']) == 1
                    assert not self.inversion
                    assert len(self.reflection_list) == 0
                    assert len(self.improper_rotation_list) == 0
                    self.point_group = f'C{self.principal_axis_order}'
        # If point group is D2d, the principal axis might be wrongly assigned.
        if self.point_group == 'D2d':
            # The principal axis is the one that is in both reflection planes.
            for axis in self.proper_rotation_list:
                if abs(np.dot(axis, self.symmetry_elements['sigma_d'][0])) < self.symmetry_threshold\
                        and abs(np.dot(axis, self.symmetry_elements['sigma_d'][1])) < self.symmetry_threshold:
                    self.principal_axis = axis
                    break
            for axis in self.proper_rotation_list:
                if abs(axis[0] - self.principal_axis[0]) < self.same_axis_threshold\
                        and abs(axis[1] - self.principal_axis[1]) < self.same_axis_threshold\
                        and abs(axis[2] - self.principal_axis[2]) < self.same_axis_threshold:
                    self.secondary_axis = axis
                    break
                elif abs(axis[0] + self.principal_axis[0]) < self.same_axis_threshold\
                        and abs(axis[1] + self.principal_axis[1]) < self.same_axis_threshold\
                        and abs(axis[2] + self.principal_axis[2]) < self.same_axis_threshold:
                    self.secondary_axis = axis
                    break

    def _find_proper_rotations(self) -> None:
        """Find all proper rotation axes in molecule."""
        proper_rotation_list = np.zeros((self.allocation_size, 3))
        proper_rotation_orders = np.zeros(self.allocation_size, dtype=np.int64)
        proper_rotation_idx = 0
        # Rotation by principal axes.
        for axis in self.principal_moment_of_inertia[1].T:
            # Rotation around principal_axis are the only one that can be above 2 for not the special point groups.
            if not self._check_if_axis_exist(axis, proper_rotation_list[:proper_rotation_idx]):
                rotation_order = self._check_proper_rotation(axis, np.arange(2, self.max_rotation_order + 1)[::-1])
                if rotation_order != 0:
                    proper_rotation_list[proper_rotation_idx] = axis
                    proper_rotation_orders[proper_rotation_idx] = rotation_order
                    proper_rotation_idx += 1
        # Rotation through single atom and origin.
        unique_axes = np.zeros((self.allocation_size, 3))
        unique_axes_idx = 0
        for group in self.atom_groups:
            for coordinate in self.atom_groups[group]:
                if np.max(np.abs(coordinate)) < self.same_axis_threshold:
                    # If atom is in origin.
                    continue
                axis = coordinate / np.linalg.norm(coordinate)
                if not self._check_if_axis_exist(axis, unique_axes[:unique_axes_idx]):
                    unique_axes[unique_axes_idx] = axis
                    unique_axes_idx += 1
        # Check all found axes.
        for axis in unique_axes[:unique_axes_idx]:
            if self._check_if_axis_exist(axis, proper_rotation_list[:proper_rotation_idx]):
                continue
            rotation_order = self._check_proper_rotation(axis, [2])
            if rotation_order != 0:
                proper_rotation_list[proper_rotation_idx] = axis
                proper_rotation_orders[proper_rotation_idx] = rotation_order
                proper_rotation_idx += 1
        # Rotation through midpoint of two atoms and origin.
        if self.special_group_type == 2:
            # Here the rotation order can be either 2 or 4.
            rotation_orders = [4, 2]
        else:
            rotation_orders = [2]
        unique_axes = np.zeros((self.allocation_size, 3))
        unique_axes_idx = 0
        for group in self.atom_groups:
            for a, coordinate_a in enumerate(self.atom_groups[group][1:], 1):
                for coordinate_b in self.atom_groups[group][:a]:
                    axis = (coordinate_a+coordinate_b) / 2
                    if np.max(np.abs(axis)) < self.same_axis_threshold:
                        # The midpoint cannot be in origin.
                        continue
                    axis = axis / np.linalg.norm(axis)
                    if np.max(np.abs(axis)) < self.same_axis_threshold:
                        continue
                    if not self._check_if_axis_exist(axis, unique_axes[:unique_axes_idx]):
                        unique_axes[unique_axes_idx] = axis
                        unique_axes_idx += 1
        # Check all found axes.
        for axis in unique_axes[:unique_axes_idx]:
            if self._check_if_axis_exist(axis, proper_rotation_list[:proper_rotation_idx]):
                continue
            rotation_order = self._check_proper_rotation(axis, rotation_orders)
            if rotation_order != 0:
                proper_rotation_list[proper_rotation_idx] = axis
                proper_rotation_orders[proper_rotation_idx] = rotation_order
                proper_rotation_idx += 1
        if self.special_group_type == 2:
            is_t = False
            is_o = False
            # Rotation through midpoint of three atoms and origin.
            # Find all unique axes through equal-lateral triangles.
            unique_axes = np.zeros((self.allocation_size, 3))
            unique_axes_idx = 0
            for group in self.atom_groups:
                for a, coordinate_a in enumerate(self.atom_groups[group][2:], 2):
                    for b, coordinate_b in enumerate(self.atom_groups[group][1:a], 1):
                        dist_ab = np.linalg.norm(coordinate_a - coordinate_b)
                        for coordinate_c in self.atom_groups[group][0:b]:
                            dist_ac = np.linalg.norm(coordinate_a - coordinate_c)
                            if abs(dist_ab - dist_ac) > self.symmetry_threshold:
                                # The sides of the triangle spanned by the three points must be even-sided.
                                continue
                            dist_bc = np.linalg.norm(coordinate_b - coordinate_c)
                            if abs(dist_ab - dist_bc) > self.symmetry_threshold:
                                # The sides of the triangle spanned by the three points must be even-sided.
                                continue
                            axis = (coordinate_a+coordinate_b+coordinate_c) / 3
                            if np.max(np.abs(axis)) < self.same_axis_threshold:
                                # The midpoint cannot be in origin.
                                continue
                            axis = axis / np.linalg.norm(axis)
                            if np.max(np.abs(axis)) < self.same_axis_threshold:
                                continue
                            if not self._check_if_axis_exist(axis, unique_axes[:unique_axes_idx]):
                                unique_axes[unique_axes_idx] = axis
                                unique_axes_idx += 1
            # Check all found axes.
            for axis in unique_axes[:unique_axes_idx]:
                if self._check_if_axis_exist(axis, proper_rotation_list[:proper_rotation_idx]):
                    continue
                rotation_order = self._check_proper_rotation(axis, [3])
                if rotation_order != 0:
                    proper_rotation_list[proper_rotation_idx] = axis
                    proper_rotation_orders[proper_rotation_idx] = rotation_order
                    proper_rotation_idx += 1
            # Checking if the point group cannot be I or Ih,
            # then there is no point in looking for C5 axes.
            num_c3 = len(proper_rotation_orders[proper_rotation_orders == 3])
            num_c2 = len(proper_rotation_orders[proper_rotation_orders == 2])
            num_c4 = len(proper_rotation_orders[proper_rotation_orders == 4])
            if num_c3 == 4 and num_c2 == 3:
                is_t = True
            if num_c4 == 3:
                is_o = True
            # Rotation through midpoint of five atoms and origin.
            # Four atoms are not needed, since these axis are all found considering midpoint of two atoms.
            if not is_t and not is_o:
                unique_axes = np.zeros((self.allocation_size, 3))
                unique_axes_idx = 0
                for group in self.atom_groups:
                    for a, coordinate_a in enumerate(self.atom_groups[group][4:], 4):
                        for b, coordinate_b in enumerate(self.atom_groups[group][3:a], 3):
                            dist_ab = np.linalg.norm(coordinate_a - coordinate_b)
                            # max_dist is the diagonal distance of a regular pentagon.
                            # (1 + np.sqrt(5)) / 2 = 1.618033988749895
                            max_dist = 1.618033988749895 * dist_ab
                            for c, coordinate_c in enumerate(self.atom_groups[group][2:b], 2):
                                dist_ac = np.linalg.norm(coordinate_a - coordinate_c)
                                if dist_ac > max_dist:
                                    continue
                                dist_bc = np.linalg.norm(coordinate_b - coordinate_c)
                                if dist_bc > max_dist:
                                    continue
                                # When three points are known, the three diagonal
                                # and the height can be determined from the side length.
                                side_length = np.min([dist_ab, dist_ac, dist_bc])
                                # diagonal = (1 + np.sqrt(5)) / 2 * side_length
                                #          = 1.618033988749895 * side_length
                                # height = np.sqrt(5 + 2 * np.sqrt(5)) / 2 * side_length
                                #        = 1.5388417685876268 * side_length
                                valid_distances = np.array([
                                    side_length, 1.618033988749895 * side_length, 1.5388417685876268 * side_length
                                ])
                                for d, coordinate_d in enumerate(self.atom_groups[group][1:c], 1):
                                    dist_ad = np.linalg.norm(coordinate_a - coordinate_d)
                                    if np.min(np.abs(valid_distances - dist_ad) > self.symmetry_threshold):
                                        continue
                                    dist_bd = np.linalg.norm(coordinate_b - coordinate_d)
                                    if np.min(np.abs(valid_distances - dist_bd) > self.symmetry_threshold):
                                        continue
                                    dist_cd = np.linalg.norm(coordinate_c - coordinate_d)
                                    if np.min(np.abs(valid_distances - dist_cd) > self.symmetry_threshold):
                                        continue
                                    for coordinate_e in self.atom_groups[group][0:d]:
                                        dist_ae = np.linalg.norm(coordinate_a - coordinate_e)
                                        if np.min(np.abs(valid_distances - dist_ae) > self.symmetry_threshold):
                                            continue
                                        dist_be = np.linalg.norm(coordinate_b - coordinate_e)
                                        if np.min(np.abs(valid_distances - dist_be) > self.symmetry_threshold):
                                            continue
                                        dist_ce = np.linalg.norm(coordinate_c - coordinate_e)
                                        if np.min(np.abs(valid_distances - dist_ce) > self.symmetry_threshold):
                                            continue
                                        dist_de = np.linalg.norm(coordinate_d - coordinate_e)
                                        if np.min(np.abs(valid_distances - dist_de) > self.symmetry_threshold):
                                            continue
                                        axis = (coordinate_a+coordinate_b+coordinate_c+coordinate_d+coordinate_e) / 5
                                        if np.max(np.abs(axis)) < self.same_axis_threshold:
                                            continue
                                        axis = axis / np.linalg.norm(axis)
                                        if np.max(np.abs(axis)) < self.same_axis_threshold:
                                            continue
                                        if not self._check_if_axis_exist(axis, unique_axes[:unique_axes_idx]):
                                            unique_axes[unique_axes_idx] = axis
                                            unique_axes_idx += 1
                # Check all found axes.
                for axis in unique_axes[:unique_axes_idx]:
                    if self._check_if_axis_exist(axis, proper_rotation_list[:proper_rotation_idx]):
                        continue
                    rotation_order = self._check_proper_rotation(axis, [5])
                    if rotation_order != 0:
                        proper_rotation_list[proper_rotation_idx] = axis
                        proper_rotation_orders[proper_rotation_idx] = rotation_order
                        proper_rotation_idx += 1
        self.proper_rotation_list = proper_rotation_list[:proper_rotation_idx]
        self.proper_rotation_orders = proper_rotation_orders[:proper_rotation_idx]

    def _find_improper_rotations(self) -> None:
        """Find all improper rotation axes in molecule."""
        improper_rotation_list = np.zeros((self.allocation_size, 3))
        improper_rotation_orders = np.zeros(self.allocation_size, dtype=np.int64)
        improper_rotation_idx = 0
        # Rotation by proper rotation axes.
        for order, axis in zip(self.proper_rotation_orders, self.proper_rotation_list):
            if not self._check_if_axis_exist(axis, improper_rotation_list[:improper_rotation_idx]):
                rotation_order = self._check_improper_rotation(axis, order)
                if rotation_order != 0:
                    improper_rotation_list[improper_rotation_idx] = axis
                    improper_rotation_orders[improper_rotation_idx] = rotation_order
                    improper_rotation_idx += 1
        self.improper_rotation_list = improper_rotation_list[:improper_rotation_idx]
        self.improper_rotation_orders = improper_rotation_orders[:improper_rotation_idx]

    def _find_reflections(self) -> None:
        """Find all reflection planes in molecule."""
        unique_normal_vectors = np.zeros((self.allocation_size, 3))
        unique_normal_vectors_idx = 0
        if self.special_group_type == 2:
            # Reflection plane spanned by vector through single atom and origin and C2 axes.
            for group in self.atom_groups:
                for coordinate in self.atom_groups[group]:
                    for c2_axis in self.symmetry_elements['C2']:
                        normal_vector = np.cross(coordinate, c2_axis)
                        if np.linalg.norm(normal_vector) < self.same_axis_threshold:
                            continue
                        normal_vector = normal_vector / np.linalg.norm(normal_vector)
                        if not self._check_if_axis_exist(normal_vector,
                                                         unique_normal_vectors[:unique_normal_vectors_idx]):
                            unique_normal_vectors[unique_normal_vectors_idx] = normal_vector
                            unique_normal_vectors_idx += 1
            # Reflection plane spanned by vector through midpoint of two atoms and origin and C2 axes.
            for group in self.atom_groups:
                for i, coordinate_i in enumerate(self.atom_groups[group][1:], 1):
                    for coordinate_j in self.atom_groups[group][0:i]:
                        axis = (coordinate_i+coordinate_j) / 2
                        if np.max(np.abs(axis)) < self.same_axis_threshold:
                            # If midpoint is in origin.
                            continue
                        # Reflection plane spanned by vector through midpoint of two atoms and origin and C2 axes.
                        for c2_axis in self.symmetry_elements['C2']:
                            normal_vector = np.cross(axis, c2_axis)
                            if np.linalg.norm(normal_vector) < self.same_axis_threshold:
                                continue
                            normal_vector = normal_vector / np.linalg.norm(normal_vector)
                            if not self._check_if_axis_exist(normal_vector,
                                                             unique_normal_vectors[:unique_normal_vectors_idx]):
                                unique_normal_vectors[unique_normal_vectors_idx] = normal_vector
                                unique_normal_vectors_idx += 1
        else:
            # Principle axes of inertia as normal vector.
            for normal_vector in self.principal_moment_of_inertia[1].T:
                if not self._check_if_axis_exist(normal_vector, unique_normal_vectors[:unique_normal_vectors_idx]):
                    unique_normal_vectors[unique_normal_vectors_idx] = normal_vector
                    unique_normal_vectors_idx += 1
            # Reflection plane spanned by proper rotation axes and principal axes of inertia.
            for axis_i in self.proper_rotation_list:
                for axis_j in self.principal_moment_of_inertia[1].T:
                    normal_vector = np.cross(axis_i, axis_j)
                    if np.linalg.norm(normal_vector) < self.same_axis_threshold:
                        continue
                    normal_vector = normal_vector / np.linalg.norm(normal_vector)
                    if not self._check_if_axis_exist(normal_vector,
                                                     unique_normal_vectors[:unique_normal_vectors_idx]):
                        unique_normal_vectors[unique_normal_vectors_idx] = normal_vector
                        unique_normal_vectors_idx += 1
            # Reflection plane spanned by vector through single atom and origin and principal axes of inertia.
            for group in self.atom_groups:
                for coordinate in self.atom_groups[group]:
                    if np.max(np.abs(coordinate)) < self.same_axis_threshold:
                        # If atom is in origin.
                        continue
                    for axis in self.principal_moment_of_inertia[1].T:
                        normal_vector = np.cross(coordinate, axis)
                        if np.linalg.norm(normal_vector) < self.same_axis_threshold:
                            continue
                        normal_vector = normal_vector / np.linalg.norm(normal_vector)
                        if not self._check_if_axis_exist(normal_vector,
                                                         unique_normal_vectors[:unique_normal_vectors_idx]):
                            unique_normal_vectors[unique_normal_vectors_idx] = normal_vector
                            unique_normal_vectors_idx += 1
            # Reflection plane spanned by vector through midpoint of two atoms
            # and origin and principal axes of inertia.
            for group in self.atom_groups:
                for i, coordinate_i in enumerate(self.atom_groups[group][1:], 1):
                    for coordinate_j in self.atom_groups[group][0:i]:
                        axis = (coordinate_i+coordinate_j) / 2
                        if np.max(np.abs(axis)) < self.same_axis_threshold:
                            # If midpoint is in origin.
                            continue
                        for inertia_axis in self.principal_moment_of_inertia[1].T:
                            normal_vector = np.cross(axis, inertia_axis)
                            if np.linalg.norm(normal_vector) < self.same_axis_threshold:
                                continue
                            normal_vector = normal_vector / np.linalg.norm(normal_vector)
                            if not self._check_if_axis_exist(normal_vector,
                                                             unique_normal_vectors[:unique_normal_vectors_idx]):
                                unique_normal_vectors[unique_normal_vectors_idx] = normal_vector
                                unique_normal_vectors_idx += 1
        reflection_list = []
        for normal_vector in unique_normal_vectors[:unique_normal_vectors_idx]:
            reflection = self._check_reflection(normal_vector)
            if reflection:
                reflection_list.append(normal_vector)
        self.reflection_list = np.array(reflection_list)

    def _assign_proper_rotations_symmetry_elements(self) -> None:
        """Assign the symmetry element for all the found proper rotations."""
        # Using temporary list, so axis can be popped when assigned.
        tmp_proper_rotation_list = self.proper_rotation_list.tolist()
        tmp_proper_rotation_orders = self.proper_rotation_orders.tolist()
        # Assigning principal axis.
        # The principal axis is the axis with the largest order of rotation.
        largest_cn = 0
        largest_idx = -1
        self.principal_axis = [False]
        for i, order in enumerate(tmp_proper_rotation_orders):
            if order > largest_cn:
                largest_cn = order
                largest_idx = i
        if largest_idx != -1:
            self.principal_axis = np.array(tmp_proper_rotation_list[largest_idx])
            self.principal_axis_order = tmp_proper_rotation_orders[largest_idx]
            if not self.special_group:
                self.symmetry_elements[f'C{largest_cn}'] = [tmp_proper_rotation_list[largest_idx]]
                tmp_proper_rotation_list.pop(largest_idx)
                tmp_proper_rotation_orders.pop(largest_idx)
        # Assigning secondary axis.
        # The secondary is the C2 axis that is perpendicular to the principal axis.
        # If more than one perpendicular C2 exists, then it is the axis that goes through most atoms.
        # Here the atoms are projected down on the plane the per C2 axes live on.
        most_atoms = 0
        largest_idx = -1
        self.secondary_axis = [False]
        for i, axis in enumerate(tmp_proper_rotation_list):
            num_atoms = 0
            for coord in self._xyz:
                u, v, w = self.principal_axis
                if (u**2 + v**2)**0.5 > 1e-5:
                    projection_mat = np.array(
                        [[u * w / (u**2 + v**2)**0.5, v * w / (u**2 + v**2)**0.5, -((u**2 + v**2)**0.5)],
                         [-v / (u**2 + v**2)**0.5, u / (u**2 + v**2)**0.5, 0], [u, v, w]])
                    projected_coord = np.dot(projection_mat, coord)
                    projected_coord[2] = 0
                    projection_mat = np.array([[u * w / (u**2 + v**2)**0.5, -v / (u**2 + v**2)**0.5, u],
                                               [v * w / (u**2 + v**2)**0.5, u / (u**2 + v**2)**0.5, v],
                                               [-((u**2 + v**2)**0.5), 0, w]])
                    projected_coord = np.dot(projection_mat, projected_coord)
                else:
                    projected_coord = np.copy(coord)
                    projected_coord[2] = 0
                if np.linalg.norm(projected_coord) > 1e-5:
                    atom_vec = projected_coord / np.linalg.norm(projected_coord)
                    if np.all(np.abs(axis - atom_vec) < self.same_axis_threshold):
                        num_atoms += 1
                    elif np.all(np.abs(axis + atom_vec) < self.same_axis_threshold):
                        num_atoms += 1
            if num_atoms > most_atoms:
                most_atoms = num_atoms
                largest_idx = i
        if largest_idx != -1:
            self.secondary_axis = tmp_proper_rotation_list[largest_idx]
            if not self.special_group:
                self.symmetry_elements['C2p'] = [tmp_proper_rotation_list[largest_idx]]
                tmp_proper_rotation_list.pop(largest_idx)
                tmp_proper_rotation_orders.pop(largest_idx)
        else:
            for i, (rotation_order, axis) in enumerate(zip(tmp_proper_rotation_orders, tmp_proper_rotation_list)):
                if order == rotation_order:
                    self.secondary_axis = axis
                    if not self.special_group:
                        self.symmetry_elements['C2p'] = [axis]
                        tmp_proper_rotation_list.pop(i)
                        tmp_proper_rotation_orders.pop(i)
                    break
        if not self.special_group:
            # Assign remaining C2 axes.
            if 'C2p' not in self.symmetry_elements:
                self.symmetry_elements['C2p'] = []
            self.symmetry_elements['C2pp'] = []
            for i in range(len(tmp_proper_rotation_list) - 1, -1, -1):
                if tmp_proper_rotation_orders[i] == 2:
                    c2p_check = False
                    for n in range(1, self.principal_axis_order):
                        rot_mat = self.get_rotation_matrix(self.principal_axis,
                                                           2 * np.pi * n / self.principal_axis_order)
                        axis = np.dot(rot_mat, tmp_proper_rotation_list[i])
                        if np.all(np.abs(axis - self.secondary_axis) < self.same_axis_threshold):
                            c2p_check = True
                            break
                        elif np.all(np.abs(axis + self.secondary_axis) < self.same_axis_threshold):
                            c2p_check = True
                            break
                    if c2p_check:
                        self.symmetry_elements['C2p'].append(tmp_proper_rotation_list[i])
                    else:
                        self.symmetry_elements['C2pp'].append(tmp_proper_rotation_list[i])
                    tmp_proper_rotation_list.pop(i)
                    tmp_proper_rotation_orders.pop(i)
        # Assign remaining Cn axes, n>2.
        # Also n=2 if special group.
        for i in range(len(tmp_proper_rotation_list) - 1, -1, -1):
            if f'C{tmp_proper_rotation_orders[i]}' not in self.symmetry_elements:
                self.symmetry_elements[f'C{tmp_proper_rotation_orders[i]}'] = [tmp_proper_rotation_list[i]]
            else:
                self.symmetry_elements[f'C{tmp_proper_rotation_orders[i]}'].append(tmp_proper_rotation_list[i])
            tmp_proper_rotation_list.pop(i)
            tmp_proper_rotation_orders.pop(i)
        assert len(tmp_proper_rotation_list) == 0
        for key in self.symmetry_elements:
            if 'C' in key:
                self.symmetry_elements[key] = np.array(self.symmetry_elements[key])

    def _assign_improper_rotations_symmetry_elements(self) -> None:
        """Assign the symmetry element for all the found improper rotations."""
        # Using temporary list, so axis can be popped when assigned.
        tmp_improper_rotation_list = self.improper_rotation_list.tolist()
        tmp_improper_rotation_orders = self.improper_rotation_orders.tolist()
        for i in range(len(tmp_improper_rotation_list) - 1, -1, -1):
            if f'S{tmp_improper_rotation_orders[i]}' not in self.symmetry_elements:
                self.symmetry_elements[f'S{tmp_improper_rotation_orders[i]}'] = [tmp_improper_rotation_list[i]]
            else:
                self.symmetry_elements[f'S{tmp_improper_rotation_orders[i]}'].append(tmp_improper_rotation_list[i])
            tmp_improper_rotation_list.pop(i)
            tmp_improper_rotation_orders.pop(i)
        assert len(tmp_improper_rotation_list) == 0
        for key in self.symmetry_elements:
            if 'S' in key:
                self.symmetry_elements[key] = np.array(self.symmetry_elements[key])

    def _assign_reflections_symmetry_elements(self) -> None:
        """Assign the symmetry element for all the found reflections."""
        temp_reflection_list = np.array(self.reflection_list).tolist()
        self.symmetry_elements['sigma_h'] = []
        self.symmetry_elements['sigma_d'] = []
        self.symmetry_elements['sigma_v'] = []
        if self.special_group and self.special_group_type == 2:
            # Assign sigma_h.
            for c2_axis in self.symmetry_elements['C2']:
                for i in range(len(temp_reflection_list) - 1, -1, -1):
                    norm_vec = np.array(temp_reflection_list[i])
                    if np.all(np.abs(norm_vec - c2_axis) < self.same_axis_threshold):
                        self.symmetry_elements['sigma_h'].append(norm_vec)
                        temp_reflection_list.pop(i)
                        break
                    elif np.all(np.abs(norm_vec + c2_axis) < self.same_axis_threshold):
                        self.symmetry_elements['sigma_h'].append(norm_vec)
                        temp_reflection_list.pop(i)
                        break
            # Assign sigma_d.
            for i in range(len(temp_reflection_list) - 1, -1, -1):
                self.symmetry_elements['sigma_d'].append(temp_reflection_list[i])
                temp_reflection_list.pop(i)
        elif self.special_group and self.special_group_type == 1:
            # Assign sigma_h.
            for cn_axis in self.symmetry_elements[f'C{self.max_rotation_order}']:
                for i in range(len(temp_reflection_list) - 1, -1, -1):
                    norm_vec = np.array(temp_reflection_list[i])
                    symmetry_element = cn_axis
                    if np.all(np.abs(norm_vec - symmetry_element) < self.same_axis_threshold):
                        self.symmetry_elements['sigma_h'].append(norm_vec)
                        temp_reflection_list.pop(i)
                        break
                    elif np.all(np.abs(norm_vec + symmetry_element) < self.same_axis_threshold):
                        self.symmetry_elements['sigma_h'].append(norm_vec)
                        temp_reflection_list.pop(i)
                        break
            # Assign sigma_v.
            for i in range(len(temp_reflection_list) - 1, -1, -1):
                self.symmetry_elements['sigma_v'].append(temp_reflection_list[i])
                temp_reflection_list.pop(i)
        else:
            # Assign sigma_h.
            for i in range(len(temp_reflection_list) - 1, -1, -1):
                norm_vec = temp_reflection_list[i]
                if self.principal_axis[0] is not False:
                    if np.all(np.abs(norm_vec - self.principal_axis) < self.same_axis_threshold):
                        self.symmetry_elements['sigma_h'] = [norm_vec]
                        temp_reflection_list.pop(i)
                        break
                    elif np.all(np.abs(norm_vec + self.principal_axis) < self.same_axis_threshold):
                        self.symmetry_elements['sigma_h'] = [norm_vec]
                        temp_reflection_list.pop(i)
                        break
                else:
                    for axis in self.principal_moment_of_inertia[1].T:
                        if np.all(np.abs(norm_vec - axis) < self.same_axis_threshold):
                            self.symmetry_elements['sigma_h'] = [norm_vec]
                            temp_reflection_list.pop(i)
                            break
                        elif np.all(np.abs(norm_vec + axis) < self.same_axis_threshold):
                            self.symmetry_elements['sigma_h'] = [norm_vec]
                            temp_reflection_list.pop(i)
                            break
            # Assign sigma_v and sigma_d.
            for i in range(len(temp_reflection_list) - 1, -1, -1):
                norm_vec = temp_reflection_list[i]
                plane_assigned_check = False
                if len(self.symmetry_elements['C2p']) == 0:
                    self.symmetry_elements['sigma_v'].append(norm_vec)
                    temp_reflection_list.pop(i)
                    plane_assigned_check = True
                else:
                    for axis in self.symmetry_elements['C2p']:
                        if abs(np.dot(axis, norm_vec)) < self.same_axis_threshold:
                            self.symmetry_elements['sigma_v'].append(norm_vec)
                            temp_reflection_list.pop(i)
                            plane_assigned_check = True
                            break
                if not plane_assigned_check:
                    # If not sigma_v then it is sigma_d.
                    self.symmetry_elements['sigma_d'].append(norm_vec)
                    temp_reflection_list.pop(i)
        assert len(temp_reflection_list) == 0
        for key in self.symmetry_elements:
            if 'sigma' in key:
                self.symmetry_elements[key] = np.array(self.symmetry_elements[key])

    def rotate_molecule_pseudo_convention(self) -> np.ndarray:
        """Rotate the molecule, so that it is suitable for Dalton's generator input.

        Returns:
            Coordinates of rotated molecule.
        """
        if self.point_group == 'Cs':
            to_z_axis = self.symmetry_elements['sigma_h'][0]
        elif self.point_group == 'Ci':
            to_z_axis = self.principal_moment_of_inertia[1][:, 2]
        elif self.point_group in ['C2', 'C4', 'C6', 'C8', 'C10', 'C12', 'C14', 'C16', 'C18', 'C20']:
            to_z_axis = self.principal_axis
        elif self.point_group in ['C3', 'C5', 'C7', 'C9', 'C11', 'C13', 'C15', 'C17', 'C19']:
            to_z_axis = self.principal_axis
        elif self.point_group in ['D2', 'D4', 'D6', 'D8', 'D10', 'D12', 'D14', 'D16', 'D18', 'D20']:
            to_z_axis = self.principal_axis
        elif self.point_group in ['D3', 'D5', 'D7', 'D9', 'D11', 'D13', 'D15', 'D17', 'D19']:
            to_z_axis = self.principal_axis
        elif self.point_group in ['C2v', 'C4v', 'C6v', 'C8v', 'C10v', 'C12v', 'C14v', 'C16v', 'C18v', 'C20v']:
            to_z_axis = self.principal_axis
        elif self.point_group in ['C3v', 'C5v', 'C7v', 'C9v', 'C11v', 'C13v', 'C15v', 'C17v', 'C19v']:
            to_z_axis = self.principal_axis
        elif self.point_group in ['C2h', 'C4h', 'C6h', 'C8h', 'C10h', 'C12h', 'C14h', 'C16h', 'C18h', 'C20h']:
            to_z_axis = self.principal_axis
        elif self.point_group in ['C3h', 'C5h', 'C7h', 'C9h', 'C11h', 'C13h', 'C15h', 'C17h', 'C19h']:
            to_z_axis = self.principal_axis
        elif self.point_group in ['D2h']:
            axis_through_num_atoms = [0, 0, 0]
            for i, axis in enumerate(self.proper_rotation_list):
                for xyz in self._xyz:
                    normed_xyz = xyz / np.linalg.norm(xyz)
                    if abs(1 - np.dot(axis, normed_xyz)) < self.symmetry_threshold:
                        axis_through_num_atoms[i] += 1
            max_idx = np.argmax(axis_through_num_atoms)
            max_atoms = axis_through_num_atoms[max_idx]
            axis_through_num_atoms[max_idx] = 0
            second_max_idx = np.argmax(axis_through_num_atoms)
            if max_atoms == axis_through_num_atoms[second_max_idx]:
                sum_distance_to_atoms = [0.0, 0.0, 0.0]
                for i, axis in enumerate(self.proper_rotation_list):
                    for xyz in self._xyz:
                        normed_xyz = xyz / np.linalg.norm(xyz)
                        sum_distance_to_atoms[i] += abs(1 - np.dot(axis, normed_xyz))\
                            * np.linalg.norm(xyz)
                max_idx = np.argmin(sum_distance_to_atoms)
                sum_distance_to_atoms[max_idx] = 1e9
                second_max_idx = np.argmin(sum_distance_to_atoms)
            to_z_axis = self.proper_rotation_list[max_idx]
        elif self.point_group in ['D4h', 'D6h', 'D8h', 'D10h', 'D12h', 'D14h', 'D16h', 'D18h', 'D20h']:
            to_z_axis = self.principal_axis
        elif self.point_group in ['D3h', 'D5h', 'D7h', 'D9h', 'D11h', 'D13h', 'D15h', 'D17h', 'D19h']:
            to_z_axis = self.principal_axis
        elif self.point_group in ['D2d', 'D4d', 'D6d', 'D8d', 'D10d', 'D12d', 'D14d', 'D16d', 'D18d', 'D20d']:
            to_z_axis = self.principal_axis
        elif self.point_group in ['D3d', 'D5d', 'D7d', 'D9d', 'D11d', 'D13d', 'D15d', 'D17d', 'D19d']:
            to_z_axis = self.principal_axis
        elif self.point_group in ['S4', 'S8', 'S12', 'S16', 'S20']:
            to_z_axis = self.principal_axis
        elif self.point_group in ['S6', 'S10', 'S14', 'S18']:
            to_z_axis = self.principal_axis
        elif self.point_group in ['T', 'Td', 'Th']:
            to_z_axis = self.symmetry_elements['C2'][0]
        elif self.point_group in ['O', 'Oh']:
            to_z_axis = self.symmetry_elements['C4'][0]
        elif self.point_group in ['I', 'Ih']:
            to_z_axis = self.symmetry_elements['C2'][0]
        elif self.point_group in ['Coov', 'Dooh']:
            to_z_axis = self.principal_axis
        elif self.point_group in ['K']:
            to_z_axis = np.array([0, 0, 1])
        # First axis to Z.
        u, v, w = to_z_axis
        if (u**2 + v**2)**0.5 > 1e-6:
            transformation_mat = np.array(
                [[u * w / (u**2 + v**2)**0.5, v * w / (u**2 + v**2)**0.5, -((u**2 + v**2)**0.5)],
                 [-v / (u**2 + v**2)**0.5, u / (u**2 + v**2)**0.5, 0], [u, v, w]])
        else:
            transformation_mat = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
        self._xyz = np.dot(transformation_mat, self._xyz.T).T
        if len(self.proper_rotation_list) != 0:
            self.proper_rotation_list = np.dot(transformation_mat, self.proper_rotation_list.T).T
        if len(self.improper_rotation_list) != 0:
            self.improper_rotation_list = np.dot(transformation_mat, self.improper_rotation_list.T).T
        if len(self.reflection_list) != 0:
            self.reflection_list = np.dot(transformation_mat, self.reflection_list.T).T
        for key in self.symmetry_elements:
            if key == 'inversion':
                continue
            if len(self.symmetry_elements[key]) != 0:
                self.symmetry_elements[key][0] = np.dot(transformation_mat, self.symmetry_elements[key][0])
        if len(self.principal_axis) == 3:
            self.principal_axis = np.dot(transformation_mat, self.principal_axis)
        if len(self.secondary_axis) == 3:
            self.secondary_axis = np.dot(transformation_mat, self.secondary_axis)
        self.moments_of_inertia = self._calculate_moment_of_inertia_tensor()
        self.principal_moment_of_inertia = self._calculate_principal_axes_of_inertia()
        # Second axis to x.
        if self.point_group == 'Cs':
            to_z_axis = self.symmetry_elements['sigma_h'][0]
            if all(abs(to_z_axis - self.principal_moment_of_inertia[1][:, 0])) > self.same_axis_threshold:
                to_x_axis = self.principal_moment_of_inertia[1][:, 0]
            else:
                to_x_axis = self.principal_moment_of_inertia[1][:, 1]
        elif self.point_group == 'Ci':
            to_z_axis = self.principal_moment_of_inertia[1][:, 2]
            to_x_axis = self.principal_moment_of_inertia[1][:, 0]
        elif self.point_group in ['C2', 'C4', 'C6', 'C8', 'C10', 'C12', 'C14', 'C16', 'C18', 'C20']:
            to_z_axis = self.principal_axis
            if all(abs(to_z_axis - self.principal_moment_of_inertia[1][:, 0])) > self.same_axis_threshold:
                to_x_axis = self.principal_moment_of_inertia[1][:, 0]
            else:
                to_x_axis = self.principal_moment_of_inertia[1][:, 1]
        elif self.point_group in ['C3', 'C5', 'C7', 'C9', 'C11', 'C13', 'C15', 'C17', 'C19']:
            to_z_axis = self.principal_axis
            if all(abs(to_z_axis - self.principal_moment_of_inertia[1][:, 0])) > self.same_axis_threshold:
                to_x_axis = self.principal_moment_of_inertia[1][:, 0]
            else:
                to_x_axis = self.principal_moment_of_inertia[1][:, 1]
        elif self.point_group in ['D2', 'D4', 'D6', 'D8', 'D10', 'D12', 'D14', 'D16', 'D18', 'D20']:
            to_x_axis = self.secondary_axis
        elif self.point_group in ['D3', 'D5', 'D7', 'D9', 'D11', 'D13', 'D15', 'D17', 'D19']:
            to_x_axis = self.symmetry_elements['C2p'][0]
        elif self.point_group in ['C2v', 'C4v', 'C6v', 'C8v', 'C10v', 'C12v', 'C14v', 'C16v', 'C18v', 'C20v']:
            # By convention this x-axis is perpendicular to the plane with most atoms in it.
            most_atoms = -1
            plane_idx = -1
            for i, sigma_v in enumerate(self.symmetry_elements['sigma_v']):
                num_atoms = 0
                for xyz in self._xyz:
                    # The atom is in the plane, if it perpendicular to the normal vector of the plane.
                    if np.dot(sigma_v, xyz) < self.symmetry_threshold:
                        num_atoms += 1
                if num_atoms > most_atoms:
                    most_atoms = num_atoms
                    plane_idx = i
            to_x_axis = self.symmetry_elements['sigma_v'][plane_idx]
        elif self.point_group in ['C3v', 'C5v', 'C7v', 'C9v', 'C11v', 'C13v', 'C15v', 'C17v', 'C19v']:
            to_x_axis = self.symmetry_elements['sigma_v'][0]
        elif self.point_group in ['C2h', 'C4h', 'C6h', 'C8h', 'C10h', 'C12h', 'C14h', 'C16h', 'C18h', 'C20h']:
            if self.point_group == 'C2h':
                to_z_axis = self.principal_axis
                if all(abs(to_z_axis - self.principal_moment_of_inertia[1][:, 0])) > self.same_axis_threshold:
                    to_x_axis = self.principal_moment_of_inertia[1][:, 0]
                else:
                    to_x_axis = self.principal_moment_of_inertia[1][:, 1]
            else:
                to_x_axis = self.secondary_axis
        elif self.point_group in ['C3h', 'C5h', 'C7h', 'C9h', 'C11h', 'C13h', 'C15h', 'C17h', 'C19h']:
            to_z_axis = self.principal_axis
            if all(abs(to_z_axis - self.principal_moment_of_inertia[1][:, 0])) > self.same_axis_threshold:
                to_x_axis = self.principal_moment_of_inertia[1][:, 0]
            else:
                to_x_axis = self.principal_moment_of_inertia[1][:, 1]
        elif self.point_group in ['D2h']:
            to_x_axis = self.proper_rotation_list[second_max_idx]
        elif self.point_group in ['D4h', 'D6h', 'D8h', 'D10h', 'D12h', 'D14h', 'D16h', 'D18h', 'D20h']:
            to_x_axis = self.symmetry_elements['C2p'][0]
        elif self.point_group in ['D3h', 'D5h', 'D7h', 'D9h', 'D11h', 'D13h', 'D15h', 'D17h', 'D19h']:
            to_x_axis = self.secondary_axis
        elif self.point_group in ['D2d', 'D4d', 'D6d', 'D8d', 'D10d', 'D12d', 'D14d', 'D16d', 'D18d', 'D20d']:
            # Have to orientate with respect to the reflection plane, instead
            # of the secondary axis for the Dalton-generator to work.
            to_x_axis = self.symmetry_elements['sigma_d'][0]
        elif self.point_group in ['D3d', 'D5d', 'D7d', 'D9d', 'D11d', 'D13d', 'D15d', 'D17d', 'D19d']:
            # Have to orientate with respect to the reflection plane, instead
            # of the secondary axis for the Dalton-generator to work.
            to_x_axis = self.symmetry_elements['sigma_d'][0]
        elif self.point_group in ['S4', 'S8', 'S12', 'S16', 'S20']:
            to_x_axis = self.symmetry_elements[self.point_group][0]
        elif self.point_group in ['S6', 'S10', 'S14', 'S18']:
            to_x_axis = self.symmetry_elements[self.point_group][0]
        elif self.point_group in ['T', 'Td', 'Th']:
            to_x_axis = self.symmetry_elements['C2'][1]
        elif self.point_group in ['O', 'Oh']:
            to_x_axis = self.symmetry_elements['C4'][1]
        elif self.point_group in ['I', 'Ih']:
            for symmetry_element in self.symmetry_elements['C2'][1:]:
                to_z_axis = self.symmetry_elements['C2'][0]
                if abs(np.dot(to_z_axis, symmetry_element)) < self.symmetry_threshold:
                    to_x_axis = symmetry_element
                    break
        elif self.point_group in ['Coov', 'Dooh']:
            to_x_axis = [1, 0, 0]
        elif self.point_group in ['K']:
            to_x_axis = np.array([1, 0, 0])
        # Doing the rotation to the x-axis.
        theta = np.arccos(np.dot(to_x_axis, [1, 0, 0]))
        transformation_mat = np.array([[np.cos(theta), -np.sin(theta), 0], [np.sin(theta),
                                                                            np.cos(theta), 0], [0, 0, 1]])
        self._xyz = np.dot(transformation_mat, self._xyz.T).T
        self._xyz[abs(self._xyz) < self.symmetry_threshold] = 0
        if len(self.proper_rotation_list) != 0:
            self.proper_rotation_list = np.dot(transformation_mat, self.proper_rotation_list.T).T
        if len(self.improper_rotation_list) != 0:
            self.improper_rotation_list = np.dot(transformation_mat, self.improper_rotation_list.T).T
        if len(self.reflection_list) != 0:
            self.reflection_list = np.dot(transformation_mat, self.reflection_list.T).T
        for key in self.symmetry_elements:
            if key == 'inversion':
                continue
            if len(self.symmetry_elements[key]) != 0:
                self.symmetry_elements[key][0] = np.dot(transformation_mat, self.symmetry_elements[key][0])
        if len(self.principal_axis) == 3:
            self.principal_axis = np.dot(transformation_mat, self.principal_axis)
        if len(self.secondary_axis) == 3:
            self.secondary_axis = np.dot(transformation_mat, self.secondary_axis)
        self.moments_of_inertia = self._calculate_moment_of_inertia_tensor()
        self.principal_moment_of_inertia = np.linalg.eigh(self.moments_of_inertia)
        return self._xyz

    def _calculate_center_of_mass(self) -> np.ndarray:
        """Calculates the center of mass.

        Returns:
            Center of mass.
        """
        return np.average(self._xyz, axis=0, weights=self.mass)

    def _calculate_moment_of_inertia_tensor(self) -> np.ndarray:
        """Calculate the moment of inertia tensor.

        Returns:
            Moment of inertia tensor.
        """
        inertia_tensor = np.zeros((3, 3))
        inertia_tensor[0, 0] = np.sum(self.mass * (self._xyz[:, 1]**2 + self._xyz[:, 2]**2))
        inertia_tensor[1, 1] = np.sum(self.mass * (self._xyz[:, 0]**2 + self._xyz[:, 2]**2))
        inertia_tensor[2, 2] = np.sum(self.mass * (self._xyz[:, 0]**2 + self._xyz[:, 1]**2))
        inertia_tensor[0, 1] = inertia_tensor[1, 0] = -np.sum(self.mass * self._xyz[:, 0] * self._xyz[:, 1])
        inertia_tensor[0, 2] = inertia_tensor[2, 0] = -np.sum(self.mass * self._xyz[:, 0] * self._xyz[:, 2])
        inertia_tensor[1, 2] = inertia_tensor[2, 1] = -np.sum(self.mass * self._xyz[:, 1] * self._xyz[:, 2])
        return inertia_tensor

    def _calculate_principal_axes_of_inertia(self) -> Tuple[np.ndarray, np.ndarray]:
        """Calculate the principal axes of moment of inertia.

        Returns:
            Principal moments and principal axes of moment of inertia.
        """
        principal_moments, principal_axes = np.linalg.eigh(self.moments_of_inertia)
        # If all the principal_moments are the same, make sure x have the
        # largest first element, and y have the largest second element.
        if np.allclose(principal_moments, principal_moments[0]):
            tmp_principal_axes = np.copy(principal_axes)
            x_idx = np.argmax(np.abs(tmp_principal_axes[0, :]))
            principal_axes[:, 0] = tmp_principal_axes[:, x_idx]
            y_idx = np.argmax(np.abs(tmp_principal_axes[1, :]))
            principal_axes[:, 1] = tmp_principal_axes[:, y_idx]
            z_idx = np.argmax(np.abs(tmp_principal_axes[2, :]))
            principal_axes[:, 2] = tmp_principal_axes[:, z_idx]
        # The largest element of the first two vectors should be positive.
        max_idx = np.argmax(np.abs(principal_axes[:, 0]))
        if principal_axes[max_idx, 0] < 0.0:
            principal_axes[:, 0] = -principal_axes[:, 0]
        max_idx = np.argmax(np.abs(principal_axes[:, 1]))
        if principal_axes[max_idx, 1] < 0.0:
            principal_axes[:, 1] = -principal_axes[:, 1]
        # Make sure that using the principal axes of inertia as a coordinate system basis,
        # follows the right hand rule.
        if not np.allclose(np.cross(principal_axes[:, 0], principal_axes[:, 1]) - principal_axes[:, 2], 0):
            principal_axes[:, 2] = -principal_axes[:, 2]
        output = namedtuple('PrincipalMomentsInertia', ['principal_moments_of_inertia', 'principal_axis'])
        return output(principal_moments, principal_axes)

    def _check_inversion(self) -> bool:
        """Checks for inversion symmetry.

        Returns:
            True if the molecule has inversion symmetry.
        """
        return self._check_if_molecule_is_same(np.diag([-1, -1, -1]))

    def _check_proper_rotation(self, axis: np.ndarray, rotation_orders: List[int]) -> int:
        """Checks if axis is symmetry axis.

        Args:
            axis: Axis of rotation.
            rotation_order: Rotation orders to test, highest must be first.

        Returns:
            Highest order of rotation for which axis is a symmetry axis.
        """
        # If rotation order is 1, then it is just identity.
        for rotation_order in rotation_orders:
            if self._check_if_molecule_is_same(self.get_rotation_matrix(axis, 2 * np.pi / rotation_order)):
                return rotation_order
        return 0

    def _check_improper_rotation(self, axis: np.ndarray, rotation_order: int) -> int:
        """Checks if axis is symmetry axis.

        Args:
            axis: Axis of rotation.
            rotation_order: Order of co-linear proper rotation axis.

        Returns:
            Highest order of rotation for which axis is a symmetry axis.
        """
        # Improper rotations are always co-linear with one of the proper rotations.
        # Improper rotations can be same order or twice the order of the co-linear proper rotation.
        # If rotation order is 1, then it is just a reflection.
        if rotation_order == 1:
            return 0
        rot_mat = self.get_rotation_matrix(axis, 2 * np.pi / (2*rotation_order))
        ref_mat = self.get_reflection_matrix(axis)
        if self._check_if_molecule_is_same(np.dot(rot_mat, ref_mat)):
            return 2 * rotation_order
        # If rotation order is 2, then it is just identity.
        if rotation_order == 2:
            return 0
        rot_mat = self.get_rotation_matrix(axis, 2 * np.pi / rotation_order)
        ref_mat = self.get_reflection_matrix(axis)
        if self._check_if_molecule_is_same(np.dot(rot_mat, ref_mat)):
            return rotation_order
        return 0

    def _check_reflection(self, norm_vector: np.ndarray) -> bool:
        """Checks if axis is symmetry axis.

        Args:
            norm_vector: Normal vector to plane of reflection.

        Returns:
            True, if plane is a reflection plane.
        """
        return self._check_if_molecule_is_same(self.get_reflection_matrix(norm_vector))

    def _check_if_group_is_same(self, group_original: np.ndarray, group_new: np.ndarray) -> bool:
        """Checks if two groups are identical within a threshold.

        Args:
            group_original: Molecule coordinates.
            group_new: Molecule coordinates.

        Returns:
            True if the molecules are identical.
        """
        same_check = True
        distance2_mat = np.sum((group_original[:, None] - group_new[None, :])**2, -1)
        for distance2_to_atoms in distance2_mat:
            if np.min(distance2_to_atoms)**0.5 < self.symmetry_threshold:
                continue
            else:
                same_check = False
                break
        return same_check

    def _check_if_molecule_is_same(self, transformation_mat: np.ndarray) -> bool:
        """Checks if two molecules are identical by iteration the groups.

        Args:
            transformation_mat: matrix that transform the molecule.

        Returns:
            True if the molecules are identical.
        """
        same_check = True
        for key in self.atom_groups:
            group = np.array(self.atom_groups[key])
            if not self._check_if_group_is_same(group, np.einsum('ij,kj->ki', transformation_mat, group)):
                same_check = False
                break
        return same_check

    def _check_if_axis_exist(self, axis: np.ndarray, operation_list: np.ndarray) -> bool:
        """Checks if a rotation axis, is already in the list of found rotation axes.

        Args:
            axis: New rotation axis.
            operation_list: List of found rotation axes.

        Returns:
            True if axis is already a known rotation axis.
        """
        if len(operation_list) == 0:
            return False
        axis = axis / np.linalg.norm(axis)
        axis_found = False
        if np.min(np.sum(np.abs(operation_list - axis), axis=1)) < self.same_axis_threshold:
            axis_found = True
        elif np.min(np.sum(np.abs(operation_list + axis), axis=1)) < self.same_axis_threshold:
            axis_found = True
        return axis_found

    def get_rotation_matrix(self, axis: np.ndarray, theta: float) -> np.ndarray:
        """Calculate rotation matrix.

        Args:
            axis: Axis of rotations. Is assumed to go through origin.
            theta: Angle of rotation.

        Returns:
            Rotation matrix.
        """
        axis = axis / np.linalg.norm(axis)
        rot_mat = np.zeros((3, 3))
        u, v, w = axis
        rot_mat[0, 0] = u**2 + (1 - u**2) * np.cos(theta)
        rot_mat[0, 1] = u * v * (1 - np.cos(theta)) - w * np.sin(theta)
        rot_mat[0, 2] = u * w * (1 - np.cos(theta)) + v * np.sin(theta)
        rot_mat[1, 0] = u * v * (1 - np.cos(theta)) + w * np.sin(theta)
        rot_mat[1, 1] = v**2 + (1 - v**2) * np.cos(theta)
        rot_mat[1, 2] = v * w * (1 - np.cos(theta)) - u * np.sin(theta)
        rot_mat[2, 0] = u * w * (1 - np.cos(theta)) - v * np.sin(theta)
        rot_mat[2, 1] = v * w * (1 - np.cos(theta)) + u * np.sin(theta)
        rot_mat[2, 2] = w**2 + (1 - w**2) * np.cos(theta)
        return rot_mat

    def get_reflection_matrix(self, normal_vector: np.ndarray) -> np.ndarray:
        """Calculate reflection matrix.

        Args:
            normal_vector: Normal vector to the reflection plane. Is assumed to
                go through origin.

        Returns:
            Reflection matrix.
        """
        normal_vector = normal_vector / np.linalg.norm(normal_vector)
        ref_mat = np.zeros((3, 3))
        cx, cy, cz = normal_vector
        ref_mat[0, 0] = 1 - 2 * cx**2
        ref_mat[0, 1] = ref_mat[1, 0] = -2 * cy * cx
        ref_mat[0, 2] = ref_mat[2, 0] = -2 * cz * cx
        ref_mat[1, 1] = 1 - 2 * cy**2
        ref_mat[1, 2] = ref_mat[2, 1] = -2 * cy * cz
        ref_mat[2, 2] = 1 - 2 * cz**2
        return ref_mat
