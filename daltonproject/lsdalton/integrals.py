#  Copyright (C)  The Dalton Project Developers.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Contact information can be found on our website: https://www.daltonproject.org/

import os
from typing import Optional, Tuple

import numpy as np
import scipy.special
from qcelemental import PhysicalConstantsContext, periodictable

from ..basis import Basis
from ..molecule import Molecule
from ..property import Property
from ..qcmethod import QCMethod
from ..utilities import unique_filename
from .program import lsdalton_input, molecule_input

constants = PhysicalConstantsContext('CODATA2018')

try:
    import pylsdalton
except ImportError:
    pylsdalton = None
else:
    # TODO perform some sanity checks, like test version compatibility?
    pass

__all__ = (
    'num_atoms',
    'num_electrons',
    'num_basis_functions',
    'num_ri_basis_functions',
    'overlap_matrix',
    'kinetic_energy_matrix',
    'nuclear_electron_attraction_matrix',
    'coulomb_matrix',
    'exchange_matrix',
    'exchange_correlation',
    'fock_matrix',
    'nuclear_energy',
    'multipole_interaction_matrix',
    'electrostatic_potential',
    'electrostatic_potential_integrals',
    'electronic_electrostatic_potential',
    'nuclear_electrostatic_potential',
    'eri',
    'eri4',
    'ri3',
    'ri2',
    'diagonal_density',
)

_STATE: Optional[str] = None


def num_atoms(molecule: Molecule, basis: Basis, qc_method: QCMethod) -> int:
    """Return the number of atoms.

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Number of atoms
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.number_atoms()


def num_electrons(molecule: Molecule, basis: Basis, qc_method: QCMethod) -> int:
    """Return the number of electrons.

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Number of electrons
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.number_electrons()


def num_basis_functions(molecule: Molecule, basis: Basis, qc_method: QCMethod) -> int:
    """Return the number of basis functions.

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Number of basis functions
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.number_basis()


def num_ri_basis_functions(molecule: Molecule, basis: Basis, qc_method: QCMethod) -> int:
    """Returns the number of auxiliary (RI) basis functions.

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Number of auxiliary (RI) basis functions
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.number_aux()


def overlap_matrix(molecule: Molecule,
                   basis: Basis,
                   qc_method: QCMethod,
                   geometric_derivative_order: int = 0,
                   magnetic_derivative_order: int = 0) -> np.ndarray:
    """Calculate the overlap matrix.

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.
        geometric_derivative_order: Order of the derivative with respect to nuclear displacements.
        magnetic_derivative_order: Order of the derivative with respect to magnetic field.

    Returns:
        Overlap matrix
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.overlap_matrix(geoOrder=geometric_derivative_order, magOrder=magnetic_derivative_order)


def kinetic_energy_matrix(molecule: Molecule,
                          basis: Basis,
                          qc_method: QCMethod,
                          geometric_derivative_order: int = 0,
                          magnetic_derivative_order: int = 0) -> np.ndarray:
    """Calculate the kinetic energy matrix.

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.
        geometric_derivative_order: Order of the derivative with respect to nuclear displacements.
        magnetic_derivative_order: Order of the derivative with respect to magnetic field.

    Returns:
        Kinetic energy matrix
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.kinetic_energy_matrix(geoOrder=geometric_derivative_order,
                                            magOrder=magnetic_derivative_order)


def nuclear_electron_attraction_matrix(molecule: Molecule,
                                       basis: Basis,
                                       qc_method: QCMethod,
                                       geometric_derivative_order: int = 0,
                                       magnetic_derivative_order: int = 0) -> np.ndarray:
    """Calculate the nuclear-electron attraction matrix.

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.
        geometric_derivative_order: Order of the derivative with respect to nuclear displacements.
        magnetic_derivative_order: Order of the derivative with respect to magnetic field.

    Returns:
        Nuclear-electron attraction matrix
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.nuclear_electron_attraction_matrix(geoOrder=geometric_derivative_order,
                                                         magOrder=magnetic_derivative_order)


def coulomb_matrix(density_matrix: np.ndarray,
                   molecule: Molecule,
                   basis: Basis,
                   qc_method: QCMethod,
                   geometric_derivative_order: int = 0,
                   magnetic_derivative_order: int = 0) -> np.ndarray:
    """Calculate the Coulomb matrix.

    Args:
        density_matrix: Density matrix
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.
        geometric_derivative_order: Order of the derivative with respect to nuclear displacements.
        magnetic_derivative_order: Order of the derivative with respect to magnetic field.

    Returns:
        Coulomb matrix
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.coulomb_matrix(density_matrix,
                                     geoOrder=geometric_derivative_order,
                                     magOrder=magnetic_derivative_order)


def exchange_matrix(density_matrix: np.ndarray,
                    molecule: Molecule,
                    basis: Basis,
                    qc_method: QCMethod,
                    geometric_derivative_order: int = 0,
                    magnetic_derivative_order: int = 0) -> np.ndarray:
    """Calculate the exchange matrix.

    Args:
        density_matrix: Density matrix.
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.
        geometric_derivative_order: Order of the derivative with respect to nuclear displacements.
        magnetic_derivative_order: Order of the derivative with respect to magnetic field.

    Returns:
        Exchange matrix
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return -pylsdalton.exchange_matrix(
        density_matrix, geoOrder=geometric_derivative_order, magOrder=magnetic_derivative_order)


def exchange_correlation(density_matrix: np.ndarray,
                         molecule: Molecule,
                         basis: Basis,
                         qc_method: QCMethod,
                         geometric_derivative_order: int = 0,
                         magnetic_derivative_order: int = 0) -> Tuple[float, np.ndarray]:
    """Calculate the exchange-correlation energy and matrix.

    Args:
        density_matrix: Density matrix.
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.
        geometric_derivative_order: Order of the derivative with respect to nuclear displacements.
        magnetic_derivative_order: Order of the derivative with respect to magnetic field.

    Returns:
        Exchange-correlation energy and matrix
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.exchange_correlation_matrix(density_matrix,
                                                  geoOrder=geometric_derivative_order,
                                                  magOrder=magnetic_derivative_order,
                                                  testElectrons=False)


def fock_matrix(density_matrix: np.ndarray,
                molecule: Molecule,
                basis: Basis,
                qc_method: QCMethod,
                geometric_derivative_order: int = 0,
                magnetic_derivative_order: int = 0) -> np.ndarray:
    """Calculate the Fock/KS matrix.

    Args:
        density_matrix: Density matrix
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.
        geometric_derivative_order: Order of the derivative with respect to nuclear displacements.
        magnetic_derivative_order: Order of the derivative with respect to magnetic field.

    Returns:
        Fock/KS matrix
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.fock_matrix(density_matrix,
                                  geoOrder=geometric_derivative_order,
                                  magOrder=magnetic_derivative_order,
                                  testElectrons=False)


def electrostatic_potential(density_matrix: np.ndarray,
                            points: np.ndarray,
                            molecule: Molecule,
                            basis: Basis,
                            qc_method: QCMethod,
                            ep_derivative_order: Tuple[int, int] = (0, 0),
                            geometric_derivative_order: int = 0,
                            magnetic_derivative_order: int = 0) -> np.ndarray:
    """Calculate the molecular electrostatic potential at a set of points.

    Derivatives of the electrostatic potential can be calculated using
    the ep_derivative_order argument, e.g., ep_derivative_order = (0, 1)
    will calculate both the zeroth- and first-order derivatives while
    ep_derivative_order = (1, 1) will calculate first-order derivatives only.

    Args:
        density_matrix: Density matrix.
        points: Set of points where the electrostatic potential will be evaluated.
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.
        ep_derivative_order: Range of orders of the derivative of the electrostatic potential.
        geometric_derivative_order: Order of the derivative with respect to nuclear displacements.
        magnetic_derivative_order: Order of the derivative with respect to magnetic field.

    Returns:
        Point-wise electronic multipol-moment potential
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    if geometric_derivative_order > 0:
        raise NotImplementedError('Geometric derivatives are not implemented for ' 'electrostatic potentials.')
    if magnetic_derivative_order > 0:
        raise NotImplementedError('Magnetic derivatives are not implemented for ' 'electrostatic potentials.')
    if ep_derivative_order[0] < 0:
        raise ValueError('Minimum EP-derivative order must be larger than or equal to zero')
    if ep_derivative_order[1] > 1:
        raise ValueError('Maximum EP-derivative order is limited to 1.')
    if ep_derivative_order[0] > ep_derivative_order[1]:
        raise ValueError('Maximum EP-derivative order must be larger than or equal to minimum EP-derivative order.')
    _set_lsdalton_state(molecule, basis, qc_method)
    electronic_ep = electronic_electrostatic_potential(density_matrix, points, molecule, basis, qc_method,
                                                       ep_derivative_order, geometric_derivative_order,
                                                       magnetic_derivative_order)
    nuclear_ep = nuclear_electrostatic_potential(points, molecule, ep_derivative_order, geometric_derivative_order,
                                                 magnetic_derivative_order)
    return electronic_ep + nuclear_ep


def electronic_electrostatic_potential(density_matrix: np.ndarray,
                                       points: np.ndarray,
                                       molecule: Molecule,
                                       basis: Basis,
                                       qc_method: QCMethod,
                                       ep_derivative_order: Tuple[int, int] = (0, 0),
                                       geometric_derivative_order: int = 0,
                                       magnetic_derivative_order: int = 0) -> np.ndarray:
    """Calculate the electronic electrostatic potential at a set of points.

    Derivatives of the electrostatic potential can be calculated using
    the ep_derivative_order argument, e.g., ep_derivative_order = (0, 1)
    will calculate both the zeroth- and first-order derivatives while
    ep_derivative_order = (1, 1) will calculate first-order derivatives only.

    Args:
        density_matrix: Density matrix.
        points: Set of points where the electrostatic potential will be evaluated.
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.
        ep_derivative_order: Range of orders of the derivative of the electrostatic potential.
        geometric_derivative_order: Order of the derivative with respect to nuclear displacements.
        magnetic_derivative_order: Order of the derivative with respect to magnetic field.

    Returns:
        Electronic electrostatic potential
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    if geometric_derivative_order > 4:
        raise NotImplementedError('Geometric derivatives higher than order 4 are not allowed for '
                                  'electronic electrostatic potential.')
    if magnetic_derivative_order > 0:
        raise NotImplementedError('Magnetic derivatives are not implemented for '
                                  'electronic electrostatic potential.')
    if ep_derivative_order[0] < 0:
        raise ValueError('Minimum EP-derivative order must be larger than or equal to zero.')
    if ep_derivative_order[0] > ep_derivative_order[1]:
        raise ValueError('Maximum EP-derivative order must be larger than or equal to minimum EP-derivative order.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.electrostatic_potential_vector(density_matrix,
                                                     points,
                                                     epOrder=ep_derivative_order,
                                                     geoOrder=geometric_derivative_order,
                                                     magOrder=magnetic_derivative_order)


def nuclear_electrostatic_potential(points: np.ndarray,
                                    molecule: Molecule,
                                    ep_derivative_order: Tuple[int, int] = (0, 0),
                                    geometric_derivative_order: int = 0,
                                    magnetic_derivative_order: int = 0) -> np.ndarray:
    """Calculate the nuclear electrostatic potential at a set of points.

    Derivatives of the electrostatic potential can be calculated using
    the ep_derivative_order argument, e.g., ep_derivative_order = (0, 1)
    will calculate both the zeroth- and first-order derivatives while
    ep_derivative_order = (1, 1) will calculate first-order derivatives only.

    Args:
        points: Set of points where the electrostatic potential will be evaluated.
        molecule: Molecule object.
        ep_derivative_order: Range of orders of the derivative of the electrostatic potential.
        geometric_derivative_order: Order of the derivative with respect to nuclear displacements.
        magnetic_derivative_order: Order of the derivative with respect to magnetic field.

    Returns:
        Nuclear electrostatic potential
    """
    if geometric_derivative_order > 0:
        raise NotImplementedError('Geometric derivatives are not implemented for '
                                  'nuclear electrostatic potentials.')
    if magnetic_derivative_order > 0:
        raise NotImplementedError('Magnetic derivatives are not implemented for '
                                  'nuclear electrostatic potentials.')
    if ep_derivative_order[0] < 0:
        raise ValueError('Minimum EP-derivative order must be larger than or equal to zero')
    if ep_derivative_order[1] > 1:
        raise ValueError('Maximum EP-derivative order is limited to 1.')
    if ep_derivative_order[0] > ep_derivative_order[1]:
        raise ValueError('Maximum EP-derivative order must be larger than or equal to minimum EP-derivative order.')
    ep0 = ep_derivative_order[0]
    ep1 = ep_derivative_order[1]
    num_ep_comp = int(scipy.special.binom(3 + ep1, ep1) - scipy.special.binom(3 + ep0, ep0 - 1))
    num_atoms = molecule.num_atoms
    num_mag = int(scipy.special.binom(3 + magnetic_derivative_order - 1, magnetic_derivative_order))
    num_geo = int(scipy.special.binom(3*num_atoms + geometric_derivative_order - 1, geometric_derivative_order))
    num_deriv = num_mag * num_geo
    num_points = points.shape[0]
    nuclear_ep = np.zeros(shape=(num_ep_comp, num_deriv, num_points), dtype=np.float64)
    if magnetic_derivative_order == 0:
        j: int = 0
        for ep in range(ep0, ep1):
            for k in range(0, 2*ep + 1):
                for i, point in enumerate(points):
                    v: float = 0.0
                    if ep == 0:
                        for element, coordinate in zip(molecule.elements, molecule.coordinates):
                            r = np.linalg.norm(coordinate - point)
                            z = periodictable.to_atomic_number(element)
                            v = v + z/r
                    elif ep == 1:
                        for element, coordinate in zip(molecule.elements, molecule.coordinates):
                            r = np.linalg.norm(coordinate - point)
                            z = periodictable.to_atomic_number(element)
                            v = v - z * coordinate[k] / r / r / r
                    nuclear_ep[j, 0, i] = v
                j = j + 1
    if num_ep_comp == 1 and num_deriv == 1:
        return nuclear_ep.reshape((num_points))
    elif num_ep_comp == 1:
        return nuclear_ep.reshape((num_deriv, num_points))
    elif num_deriv == 1:
        return nuclear_ep.reshape((num_ep_comp, num_points))
    else:
        return nuclear_ep


def nuclear_energy(molecule: Molecule) -> float:
    """Calculate the nuclear-repulsion energy

    Args:
        molecule: Molecule object.

    Return:
        Nuclear-repulsion energy
    """
    energy: float = 0.0
    num_atoms = molecule.num_atoms
    for iatom in range(num_atoms):
        icoord = molecule.coordinates[iatom]
        iz = periodictable.to_atomic_number(molecule.elements[iatom])
        for jatom in range(iatom + 1, num_atoms):
            jcoord = molecule.coordinates[jatom]
            jz = periodictable.to_atomic_number(molecule.elements[jatom])
            energy = energy + iz * jz / np.linalg.norm(jcoord - icoord)
    return energy * constants.bohr2angstroms


def multipole_interaction_matrix(moments: np.ndarray,
                                 points: np.ndarray,
                                 molecule: Molecule,
                                 basis: Basis,
                                 qc_method: QCMethod,
                                 multipole_orders: Tuple[int, int] = (0, 0),
                                 geometric_derivative_order: int = 0,
                                 magnetic_derivative_order: int = 0) -> np.ndarray:
    """Calculate the electron-multipole electrostatic interaction matrix in atomic-orbital (AO) basis.

    Minimum and maximum orders of the multipole moments are provided by the
    multipole_orders argument, e.g., multipole_orders = (0, 1) includes
    charges (order 0) and dipoles (order 1) while multipole_orders = (1, 1)
    includes only dipoles.

    Args:
        moments: Multipole moments.
        points: Positions of the multipole moments.
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.
        multipole_orders: Minimum and maximum orders of the multipole moments, where 0 = charge, 1 = dipole, etc.
        geometric_derivative_order: Order of the derivative with respect to nuclear displacements.
        magnetic_derivative_order: Order of the derivative with respect to magnetic field.

    Returns:
        Electron-multipole electrostatic interaction matrix
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    if geometric_derivative_order > 4:
        raise NotImplementedError('Geometric derivatives higher than order 4 are not allowed for '
                                  'multipole interaction matrix.')
    if magnetic_derivative_order > 0:
        raise NotImplementedError('Magnetic derivatives are not implemented for ' 'multipole interaction matrix.')
    if multipole_orders[0] < 0:
        raise ValueError('Minimum multipole order must be larger than or equal to zero.')
    if multipole_orders[0] > multipole_orders[1]:
        raise ValueError('Maximum multipole order must be larger than or equal to minimum multipole order.')
    return pylsdalton.electrostatic_potential_matrix(moments,
                                                     points,
                                                     epOrder=multipole_orders,
                                                     geoOrder=geometric_derivative_order,
                                                     magOrder=magnetic_derivative_order)


def electrostatic_potential_integrals(points: np.ndarray,
                                      molecule: Molecule,
                                      basis: Basis,
                                      qc_method: QCMethod,
                                      ep_derivative_order: Tuple[int, int] = (0, 0),
                                      geometric_derivative_order: int = 0,
                                      magnetic_derivative_order: int = 0) -> np.ndarray:
    r"""Calculate electrostatic-potential integrals.

    Derivatives of the electrostatic potential can be calculated using
    the ep_derivative_order argument, e.g., ep_derivative_order = (0, 1)
    will calculate both the zeroth- and first-order derivatives while
    ep_derivative_order = (1, 1) will calculate first-order derivatives only.


    Let a and b be AO basis functions, C a point, and :math:`\xi` and :math:`\zeta` Cartesian components, then
    order 0: :math:`(ab|C) = \int a(r) b(r) V(r,C) dr`, with the potential: :math:`V(r,C) = 1/|r-C|`
    order 1: :math:`\int a(r) b(r) V_\xi(r,C) dr`, with first-order potential derivative:
    :math:`V_\xi(r,C) = \partial{V(r,C)}{\partial{C_\xi}}= C_\xi/|r-C|^3`
    order 2: :math:`\int a(r) b(r) V_{\xi,\zeta}(r,C) dr`, with second-order potential derivative:
    :math:`V_{\xi,\zeta}(r,C) = \partial^2{V(r,C)}{\partial{C_\xi}\partial{C_\zeta}}`

    Args:
        points: Set of points where the electrostatic-potential integrals will be evaluated.
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.
        ep_derivative_order: Range of orders of the derivative of the electrostatic potential.
        geometric_derivative_order: Order of the derivative with respect to nuclear displacements.
        magnetic_derivative_order: Order of the derivative with respect to magnetic field.

    Returns:
        Electrostatic-potential integrals
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    if geometric_derivative_order > 4:
        raise NotImplementedError('Geometric derivatives higher than order 4 are not allowed for '
                                  'electrostatic potential integrals.')
    if magnetic_derivative_order > 0:
        raise NotImplementedError('Magnetic derivatives are not implemented for '
                                  'electrostatic potential integrals.')
    if ep_derivative_order[0] < 0:
        raise ValueError('Minimum EP-derivative order must be larger than or equal to zero.')
    if ep_derivative_order[0] > ep_derivative_order[1]:
        raise ValueError('Maximum EP-derivative order must be larger than or equal to minimum EP-derivative order.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.electrostatic_potential_integrals(points,
                                                        epOrder=ep_derivative_order,
                                                        geoOrder=geometric_derivative_order,
                                                        magOrder=magnetic_derivative_order)


def eri(specs: str,
        molecule: Molecule,
        basis: Basis,
        qc_method: QCMethod,
        geometric_derivative_order: int = 0,
        magnetic_derivative_order: int = 0) -> np.ndarray:
    r"""Calculate electron-repulsion integrals for different operator choices.

    .. math::
        g_{ijkl} = \int\mathrm{d}\mathbf{r}\int\mathrm{d}\mathbf{r}^{\prime}
            \chi_{i}(\mathbf{r})\chi_{j}(\mathbf{r}^\prime)
            g(\mathbf{r},\mathbf{r}^\prime)
            \chi_{k}(\mathbf{r})\chi_{l}(\mathbf{r}^\prime)

    The first character of the `specs` string specifies the operator
    :math:`g(\mathbf{r}, \mathbf{r}^\prime)`. Valid values are:

        * C for Coulomb (:math:`g(\mathbf{r}, \mathbf{r}^\prime) = \frac{1}{|\mathbf{r} - \mathbf{r}^\prime|}`)
        * G for Gaussian geminal (:math:`g`)
        * F geminal divided by the Coulomb operator (:math:`\frac{g}{|\mathbf{r} - \mathbf{r}^\prime|}`)
        * D double commutator (:math:`[[T,g],g]`)
        * 2 Gaussian geminal operator squared (:math:`g^2`)

    The last four characters of the `specs` string specify the AO basis to use
    for the four basis functions :math:`\chi_{i}`, :math:`\chi_{j}`,
    :math:`\chi_{k}`, and :math:`\chi_{l}`. Valid values are:

        * R for regular AO basis
        * D for auxiliary (RI) AO basis
        * E for empty AO basis (i.e. for 2- and 3-center ERIs)
        * N for nuclei

    Args:
        specs: A 5-character-long string with the specification for AO basis
            and operator to use (see description above).
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.
        geometric_derivative_order: Order of the derivative with respect to nuclear displacements.
        magnetic_derivative_order: Order of the derivative with respect to magnetic field.

    Returns:
        Electron-repulsion integral tensor.
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    specs_fortran = specs[::-1]
    return pylsdalton.eri(specs_fortran, geoOrder=geometric_derivative_order, magOrder=magnetic_derivative_order)


def eri4(molecule: Molecule,
         basis: Basis,
         qc_method: QCMethod,
         geometric_derivative_order: int = 0,
         magnetic_derivative_order: int = 0) -> np.ndarray:
    r"""Calculate four-center electron-repulsion integrals.

    .. math::
        (ab|cd) = g_{acbd}

    with

    .. math::
      g(\mathbf{r},\mathbf{r}^\prime) = \frac{1}{|\mathbf{r}-\mathbf{r}^\prime|}

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.
        geometric_derivative_order: Order of the derivative with respect to nuclear displacements.
        magnetic_derivative_order: Order of the derivative with respect to magnetic field.

    Returns:
        Four-center electron-repulsion integral tensor.
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    return eri('CRRRR',
               molecule,
               basis,
               qc_method,
               geometric_derivative_order=geometric_derivative_order,
               magnetic_derivative_order=magnetic_derivative_order)


def ri3(molecule: Molecule, basis: Basis, qc_method: QCMethod) -> np.ndarray:
    r"""Calculate three-center electron-repulsion integrals.

    .. math::
        (ab|I) = g_{a0bI}

    with

    .. math::
        g(\mathbf{r},\mathbf{r}^\prime) = \frac{1}{|\mathbf{r}-\mathbf{r}^\prime|}

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Three-center RI integrals
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    return eri('CRRDE', molecule, basis, qc_method)


def ri2(molecule: Molecule, basis: Basis, qc_method: QCMethod) -> np.ndarray:
    r"""Calculate two-center electron-repulsion integrals.

    .. math::

        (I|J) = g_{I00J}

    with

    .. math::

        g(\mathbf{r},\mathbf{r}^\prime) = \frac{1}{|\mathbf{r}-\mathbf{r}^\prime|}

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Two-center RI integrals
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    return eri('CDEDE', molecule, basis, qc_method)


def diagonal_density(hamiltonian: np.ndarray, metric: np.ndarray, molecule: Molecule, basis: Basis,
                     qc_method: QCMethod) -> np.ndarray:
    """Form an AO density matrix from occupied MOs through diagonalization.

    Args:
        hamiltonian: Hamiltonian matrix.
        metric: Metric matrix (i.e. overlap matrix)
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.

    Returns:
        Density matrix
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    _set_lsdalton_state(molecule, basis, qc_method)
    return pylsdalton.diagonal_density(hamiltonian, metric)


def _set_lsdalton_state(molecule: Molecule, basis: Basis, qc_method: QCMethod) -> None:
    """Control the PyLSDalton instance.

    Create instance if it does not already exist. If it does exist, then ensure
    that the input is the same, otherwise destroy it and create a new instance.

    Args:
        molecule: Molecule object.
        basis: Basis object.
        qc_method: QCMethod object.
    """
    if not pylsdalton:
        raise Exception('PyLSDalton is not available.')
    global _STATE
    if not isinstance(molecule, Molecule):
        raise TypeError('molecule must be an instance of the Molecule class')
    if not isinstance(basis, Basis):
        raise TypeError('basis must be an instance of the Basis class')
    if not isinstance(qc_method, QCMethod):
        raise TypeError('qc_method must be a instance of the QCMethod class')
    properties = Property()
    dal_input = lsdalton_input(qc_method, properties)
    mol_input = molecule_input(molecule, basis)
    # TODO temporary fix until PyLSDalton uses CWD to find basis sets
    os.environ['BASDIR'] = os.getcwd()
    basis.write()
    filename = unique_filename([dal_input, mol_input])
    if _STATE != filename:
        if _STATE is not None:
            pylsdalton.destroy()
        _STATE = filename
        with open(f'{filename}.mol', 'w') as mol_file:
            mol_file.write(mol_input)
        with open(f'{filename}.dal', 'w') as dal_file:
            dal_file.write(dal_input)
        pylsdalton.create(dalfile=f'{filename}.dal', molfile=f'{filename}.mol')
