import daltonproject as dp
import numpy as np

molecule = dp.Molecule(input_file='data/water.xyz')
basis = dp.Basis(basis='cc-pvdz')

ccsd = dp.QCMethod('CCSD')
prop = dp.Property(excitation_energies=True, vector_potential=True)
prop.excitation_energies(states=7)
result = dp.molcas.compute(molecule, basis, ccsd, prop)

ma = dp.molcas.Arrays(result)

x = ma.position(0)
y = ma.position(1)
z = ma.position(2)
px = ma.momentum(0)
py = ma.momentum(1)
pz = ma.momentum(2)
s = ma.s

cosp_x = ma.cosp(0)
sinp_y = ma.sinp(1)
expp_z = ma.expp(2)

cos2 = ma.cos2
sin2 = ma.sin2
