import daltonproject as dp

molecule = dp.Molecule(input_file='data/water.xyz')
basis = dp.Basis(basis='cc-pVDZ')

ccsd = dp.QCMethod('CCSD')
prop = dp.Property(response_vectors=True)
prop.excitation_energies(states=14)
result = dp.dalton.compute(molecule, basis, ccsd, prop)

da = dp.dalton.Arrays(result)
# core hamiltonian
h = da.h
# overlap
s = da.s
# electron repulsion integrals
u = da.u
# MO coefficients
c = da.c

# state energies, ground and excited states
energies = da.state_energies

# position integrals
x = da.position(0)
y = da.position(1)
z = da.position(2)

# momentum integrals
px = da.momentum(0)
py = da.momentum(1)
pz = da.momentum(2)

# ground-state singles and doubles amplitudes
t1 = da.t1
t2 = da.t2

# ground-state singles and doubles Lagrange multipliers
l1 = da.l1
l2 = da.l2


# left EOMCC eigenvectors, singles and doubles, first excited state
l_single1 = da.L1(1)
l_double1 = da.L2(1)

# right EOMCC eigenvectors, singles and doubles, first excited state
r_single1 = da.R1(1)
r_double1 = da.R2(1)

# M vector for second excited state, singles and doubles
m_single1 = da.M1(2)
m_double1 = da.M2(2)
