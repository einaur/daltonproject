import daltonproject as dp

# First we setup all the basic objects, i.e., molecule,basis sets, QC methods,
# and properties.
water = dp.Molecule(input_file='./data/water.xyz')
small_basis = dp.Basis(basis='pcseg-0')
larger_basis = dp.Basis(basis='pcseg-1')
augmented_basis = dp.Basis(basis='aug-pcseg-1')
dft = dp.QCMethod('DFT', 'B3LYP')
geo_opt = dp.Property(geometry_optimization=True)
tpa = dp.Property(two_photon_absorption=True)

# Before we start, we can print the initial geometry
print('Geometry before pre-optimization')
print(water.coordinates)

# First we pre-optimize the geometry using a small basis set. For this, we use
# the interface to LSDalton
result = dp.lsdalton.compute(water, small_basis, dft, geo_opt)

# The resulting geometry and energy can be printed
print('Geometry and energy after pre-optimization')
print(result.final_geometry)
print(result.energy)

# We then want to refine the geometry using a larger basis set but first we
# need to update the geometry of the molecule
water.coordinates = result.final_geometry

# Then we run the optimization
result = dp.lsdalton.compute(water, larger_basis, dft, geo_opt)

# We can again inspect the geometry and energy
print('Geometry and energy after final optimization')
print(result.final_geometry)
print(result.energy)

# Finally, we use the final optimized structure to run a two-photon absorption
# calculation using Dalton. For this calculation we want to use a basis set
# augmented with diffuse functions.
water.coordinates = result.final_geometry
tpa_result = dp.dalton.compute(water, augmented_basis, dft, tpa)

# From this calculation we obtained the two-photon absorption transition
# tensors, strengths, and cross sections
print('The two-photon transition tensors')
print(tpa_result.two_photon_tensors)
print('The two-photon transition strengths')
print(tpa_result.two_photon_strengths)
print('The two-photon transition cross sections')
print(tpa_result.two_photon_cross_sections)
